# Mittelbach2019a

#Summary

This is the DuMux module containing the code for producing the results published in:
Maren Mittelbach Development of interface criteria for model reduction strategies for the simulation of hydrogen storage Master's thesis, Institut für Wasser- und Umweltsystemmodellierung, Universität Stuttgart, 8/2019


#Installation

DuMuX Version: dumux-course-2018(tag)

How to install?: install DuMuX like described in the user handbook and --git checkout dumux-course-2018--

Dune (dune-common, dune-istl, dune-localfunctions, dune-uggrid, dune-geometry, dune-common) Versions: 2.6 , install like described in the dumux user handbook.


CMake Version: 3.5.1

git clone https:://git.iws.uni-stuttgart.de/dumux-pub/Mittelbach2019a.git



#Results

Results on the rectangular grid

For rectangular grid: (Figure 6.2/6.3/6.4/6.5/6.6/6.7/6.8/6.9/6.10/6.11)

run rechteck/test_2p2c_injection_tpfa


Results on an anticline grid

For anticline grid: (Figure 6.13/6.14/6.15/6.16)

run dome/test_2p2c_injection_box
