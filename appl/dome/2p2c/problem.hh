// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 */

#ifndef DUMUX_INJECTION_PROBLEM_HH
#define DUMUX_INJECTION_PROBLEM_HH

#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_DUNE_ALUGRID, HAVE_UG

#include <dumux/io/gnuplotinterface.hh>

#include <dumux/discretization/cellcentered/mpfa/omethod/staticinteractionvolume.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p2c/model.hh>
#include <dumux/material/fluidsystems/brineh2.hh>


#include "spatialparams.hh"

namespace Dumux {

#ifndef ENABLECACHING
#define ENABLECACHING 0
#endif

template <class TypeTag>
class InjectionProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct Injection { using InheritsFrom = std::tuple<TwoPTwoC>; };
struct InjectionBox { using InheritsFrom = std::tuple<Injection, BoxModel>; };
struct InjectionCCTpfa { using InheritsFrom = std::tuple<Injection, CCTpfaModel>; };
struct InjectionCCMpfa { using InheritsFrom = std::tuple<Injection, CCMpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Injection> { using type = Dune::/*YaspGrid<2*/ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Injection> { using type = InjectionProblem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Injection>
{
     using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::BrineH2<Scalar, Components::Brine<Scalar>>;
//     using type = FluidSystems::BrineH2<GetPropType<TypeTag, Properties::Scalar>,
//                                      FluidSystems::BrineH2DefaultPolicy</*fastButSimplifiedRelations=*/true>>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Injection>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = InjectionSpatialParams<FVGridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::Injection> { static constexpr bool value = false; };

// Enable caching or not (reference solutions created without caching)
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };

// use the static interaction volume around interior vertices in the mpfa test
template<class TypeTag>
struct PrimaryInteractionVolume<TypeTag, TTag::InjectionCCMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;

    // structured two-d grid
    static constexpr int numIvScvs = 4;
    static constexpr int numIvScvfs = 4;

    // use the default traits
    using Traits = CCMpfaODefaultStaticInteractionVolumeTraits< NodalIndexSet, Scalar, numIvScvs, numIvScvfs >;
public:
    using type = CCMpfaOStaticInteractionVolume< Traits >;
};
} // end namespace Properties

/*!
 * \ingroup TwoPTwoCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 *
 * The domain is sized 60m times 40m and consists of two layers, a moderately
 * permeable one (\f$ K=10e-12\f$) for \f$ y<22m\f$ and one with a lower
 * permeablility (\f$ K=10e-13\f$) in the rest of the domain.
 *
 * A mixture of Nitrogen and Water vapor, which is composed according to the
 * prevailing conditions (temperature, pressure) enters a water-filled aquifer.
 * This is realized with a solution-dependent Neumann boundary condition at the
 * right boundary (\f$ 5m<y<15m\f$). The aquifer is situated 2700m below sea level.
 * The injected fluid phase migrates upwards due to buoyancy.
 * It accumulates and partially enters the lower permeable aquitard.
 *
 * The model is able to use either mole or mass fractions. The property useMoles
 * can be set to either true or false in the problem file. Make sure that the
 * according units are used in the problem set-up.
 * The default setting for useMoles is true.
 *
 * This problem uses the \ref TwoPTwoCModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p2c</tt> or
 * <tt>./test_cc2p2c</tt>
 */
template <class TypeTag>
class InjectionProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
//     using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;



    // equation indices
    enum
    {
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::BrineIdx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::H2Idx,
    };

    // phase indices
    enum
    {
        gasPhaseIdx = FluidSystem::H2Idx,
        BrineIdx = FluidSystem::BrineIdx,
        H2Idx = FluidSystem::H2Idx
    };
    
      enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wCompIdx = FluidSystem::BrineIdx,
        nCompIdx = FluidSystem::H2Idx,

        wPhaseIdx = FluidSystem::BrineIdx,
        nPhaseIdx = FluidSystem::H2Idx,

        // Phase State
        wPhaseOnly = Indices::firstPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

    };
    


    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
    
        using CellArray = std::array<unsigned int, dimWorld>;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;
     enum { dofCodim = isBox ? dim : 0 };
       
    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = ModelTraits::useMoles();

public:
    InjectionProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
         nTemperature_       = getParam<int>("Problem.NTemperature");
        nPressure_          = getParam<int>("Problem.NPressure");
        pressureLow_        = getParam<Scalar>("Problem.PressureLow");
        pressureHigh_       = getParam<Scalar>("Problem.PressureHigh");
        temperatureLow_     = getParam<Scalar>("Problem.TemperatureLow");
        temperatureHigh_    = getParam<Scalar>("Problem.TemperatureHigh");
        temperature_        = getParam<Scalar>("Problem.InitialTemperature");
        depthBOR_           = getParam<Scalar>("Problem.DepthBOR");
        name_               = getParam<std::string>("Problem.Name");
         maxDepth_=depthBOR_;

//         stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole-fractions"<<std::endl;
        else
            std::cout<<"problem uses mass-fractions"<<std::endl;


        system("rm *.out"); //alle files.out löschen
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K]
     */
    Scalar temperature() const
    { return temperature_; }

    Scalar setTimestepindex( Scalar timestepindex)
    { timestepidx_=timestepindex;
        return timestepidx_
        ;}

    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {  
        BoundaryTypes bcTypes;
        Scalar right = this->fvGridGeometry().bBoxMax()[0];

        if(globalPos[0] > right - eps_ || globalPos[0] < eps_)//Dirichlet rechts und links für den Druck
            bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();
        
   
        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.Dirichlet
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        
        return values;
    }

    
    //! \copydoc Dumux::FVProblem::source()
    NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scv.dofPosition();


      Scalar cellHeight = 0.41;// für gebogenes Gitter
      Scalar effectiveWellRadius = 0.2*cellHeight;
      Scalar WellDiameter = 0.5335;
      Scalar permeability = this->spatialParams().permeabilityAtPos(globalPos);

      Scalar mobilityGas = std::max(0.1, elemVolVars[scv].mobility(nPhaseIdx));
      Scalar mobilityBrine = std::max(0.1, elemVolVars[scv].mobility(wPhaseIdx));
      Scalar WellIndex =(2.0*M_PI*cellHeight)/log(effectiveWellRadius/WellDiameter);

      Scalar densityNw = std::max(elemVolVars[scv].density(nPhaseIdx), 6.0);
      Scalar densityW = std::max(elemVolVars[scv].density(wPhaseIdx), 800.0);
     
      
       if (globalPos[0]> 49.5&& globalPos[0] <50.5 && globalPos[1]>20&& globalPos[1]<20.5) //source in the middle of the domain
        {      
 //timesteps so gewählt, dass kein Gas rausläuft und plume sich wieder oben sammelt bevor extrahiert wird

            if(timestepidx_<23.01||(timestepidx_>50.01&& timestepidx_<55.01)||(timestepidx_>77.01&&timestepidx_<83.01)||(timestepidx_>107.01&&timestepidx_<112.01)||(timestepidx_>135.01&&timestepidx_<139.01))
            {injectionPressure_=0.7e7;
               values[contiNEqIdx] = WellIndex*permeability*mobilityGas*densityNw*(elemVolVars[scv].pressure(nPhaseIdx) - injectionPressure_)*(1.0/scv.geometry().volume());
                
                      std::cout << "massinjection " << massInjected_;
                      std::cout << "time " << timestepidx_ << " " << maxDepth_<< std::endl;
            }

            else if ((timestepidx_>42.01&&timestepidx_<46.01)||( timestepidx_>75.01&&timestepidx_<79.01)||(timestepidx_>100.01&&timestepidx_<104.01)||(timestepidx_>130.01&&timestepidx_<133.01)||(timestepidx_>160.01&&timestepidx_<165.01))
           {  values[contiNEqIdx]=-0.00001*(1.0/scv.geometry().volume());

                std::cout << "massextraction " << massInjected_ <<std::endl;
                std::cout << "time " << timestepidx_ << std::endl;

            }

           else
           {values[contiNEqIdx]=0.0;
                          std::cout << "pause" << massInjected_ <<std::endl;
           }
        }
           
        
        return values;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }

    //adds fields to paraview output
    template<class VTKWriter>
void addVtkFields(VTKWriter& vtk)
    {
        const auto& gridView = this->fvGridGeometry().gridView();
        Kxx_.resize(gridView.size(dofCodim));
        Kyy_.resize(gridView.size(dofCodim));
        D_h2_.resize(gridView.size(dofCodim));
        mu_Brine_.resize(gridView.size(dofCodim));

        vtk.addField(Kxx_, "Knw");
        vtk.addField(Kyy_, "Kw");
        vtk.addField(D_h2_, "D_H2");
        vtk.addField(mu_Brine_, "mu_liq");
    }
    
void updateVtkFields(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto& globalPos = scv.dofPosition();
                const auto dofIdxGlobal = scv.dofIndex();

                Kxx_[dofIdxGlobal] = volVars.relativePermeability(nPhaseIdx);
                Kyy_[dofIdxGlobal] = volVars.relativePermeability(wPhaseIdx);
                D_h2_[dofIdxGlobal] = volVars.diffusionCoefficient(wPhaseIdx,nCompIdx);
                mu_Brine_[dofIdxGlobal] = volVars.viscosity(wPhaseIdx);
            }
        }
    }

  void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time,
                      const Scalar timeStepSize,
                    const Scalar timeStepIndex
                   )
                   
    {   NumEqVector values;
      Scalar mass;
    Scalar DissMass=0.0;
    Scalar volume=0.0;
    Scalar volumeBrine=0.0;
    Scalar startmasse=0.0;
    Scalar DissMasscompl=0.0;
    Scalar timestepsize= time-oldTime_;
    Scalar t= time+timeStepSize;
    std::cout << timestepsize << " " << timeStepSize << " " << time << " " << t<< std::endl;


    for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {const GlobalPosition& globalPos = element.geometry().center();
                Scalar porosity = this->spatialParams().porosityAtPos(globalPos);
                
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);
                
                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);
                for (auto&& scv : scvs(fvGeometry))
                {   const auto& volVars = elemVolVars[scv];
                    Scalar massFractionH2= volVars.massFraction(wPhaseIdx, nCompIdx);
                    Scalar densityW = 1014;
                    Scalar densityNw = 0.09;
                    Scalar plumeDistance = 15.0;

                    if(globalPos[1] > plumeDistance && timeStepIndex==0.0)
                    {Scalar entryP = 1e5;//has to be as in SpatialParams
                    Scalar lambda = 2.0;//has to be as in SpatialParams
                    Scalar resSatW = 0.1;//has to be as in SpatialParams
                    Scalar resSatN = 0.0;//has to be as in SpatialParams
                    startmasse += (1.0-(std::pow(((globalPos[1]-plumeDistance)*(densityW-densityNw)*this->gravity().two_norm()+entryP),(-lambda))
                    * std::pow(entryP,lambda) * (1.0-resSatW-resSatN)+resSatW))*densityW*massFractionH2*porosity*scv.volume();//only works for Brooks-Corey
                    }
                   
                    
                    if (globalPos[0]> 49.5&& globalPos[0] <50.5  && globalPos[1] >20 && globalPos[1]<  20.5)
                    {values = source(element, fvGeometry, elemVolVars, scv);
                    mass = values[contiNEqIdx] * scv.volume()*timeStepSize;
                
                    massInjected_+=mass; //accumulates injected mass
                    }
                    volume+=scv.volume(); //gesamtvolumen der domain


                    auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
                    const auto& priVars = elemSol[scv.localDofIndex()];
                    const auto phasePresence = priVars.state();

                    if(phasePresence==1)
                {

                    DissMass += volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wPhaseIdx)
                    * scv.volume() * volVars.saturation(wPhaseIdx)*porosity;        //dissolved H2 beyond the plume
                }
                DissMasscompl += volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wPhaseIdx)
                * scv.volume() * volVars.saturation(wPhaseIdx)*porosity;  //total dissolved H2
                }
            }

            Scalar time_plot = t/3600.0;
            Scalar massInjected_plot = massInjected_;
            Scalar prozent= 100/massInjected_*DissMass;

            std::cout << " massinjected   " << massInjected_<< std::endl;
            std::cout << " startmasse   " << startmasse<< std::endl;
            std::cout << " dissmass  " << DissMass<< std::endl;
            std::cout << " volume   " << volume<< std::endl;

            //write data to mass-plot
            std::ostringstream oss;
            oss << "Masse_.out";
            std::string fileName = oss.str();
            outputFile_.open(fileName, std::ios::app);
            outputFile_ << time_plot << " " << massInjected_plot<< " " << prozent << " " << DissMass << " " << DissMasscompl<< std::endl;
            outputFile_.close();

            oldTime_=time;

    }
    

private:
    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * The internal method for the initial condition
     *
     * \param globalPos The global position
     */

    PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        Scalar densityW = 1014.0;
        Scalar densityNw = 0.09;
        priVars[pressureIdx] = 1e5 + (500 - globalPos[1])*densityW*9.81;
        Scalar plumeDistance = 18.0;

        if(globalPos[1] < plumeDistance)
        {priVars.setState(wPhaseOnly);
        priVars[switchIdx] = 0.0;
        }
              else
              {
                  priVars.setState(bothPhases);
            Scalar entryP = 1e5;//has to be as in SpatialParams
            Scalar lambda = 2.0;//has to be as in SpatialParams
            Scalar resSatW = 0.1;//has to be as in SpatialParams
            Scalar resSatN = 0.0;//has to be as in SpatialParams
            priVars[switchIdx] = 1.0-(std::pow(((globalPos[1]-plumeDistance)*(densityW-densityNw)*this->gravity().two_norm()+entryP),(-lambda))
                                    * std::pow(entryP,lambda) * (1.0-resSatW-resSatN)+resSatW);//only works for Brooks-Corey

              }


    return priVars;
     }
    Scalar inflow;
    Scalar maxDepth_;
    std::string name_;
    Scalar massInjected_;
    Scalar timestepidx_;
    Scalar massExtracted_;
    Scalar mutable injectionPressure_;
    Scalar energyInjected_;
    Scalar massInjectedTimestep_;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;

    Scalar temperature_;
    Scalar depthBOR_;
    static constexpr Scalar eps_ = 1e-6;

    int nTemperature_;
    int nPressure_;
    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
        std::multimap<int, Element> mapColumns_;
    int numberOfColumns_;
    std::ofstream outputFile_;
    std::ofstream OutputFile_;
    std::vector<double> Kxx_;
    std::vector<double> Kyy_;
    std::vector<double> D_h2_;
    std::vector<double> mu_Brine_;
    Scalar dummy_;
    Scalar oldTime_;

};



} // end namespace Dumux

#endif
