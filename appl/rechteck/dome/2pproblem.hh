// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \ingroup TwoPTests
 * \brief The properties for the incompressible 2p test.
 */
#ifndef DUMUX_INCOMPRESSIBLE_TWOP_TEST_PROBLEM_HH
#define DUMUX_INCOMPRESSIBLE_TWOP_TEST_PROBLEM_HH

#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_DUNE_ALUGRID, HAVE_UG

#include <dumux/discretization/box.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/ccmpfa.hh>

#include <dumux/material/fluidsystems/1pliquid.hh>
#include <dumux/material/fluidsystems/1pgas.hh>
#include <dumux/material/components/brine.hh>
#include <dumux/material/components/h2.hh>

#include <dumux/material/fluidsystems/2pimmiscible.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p/model.hh>

#include <dumux/io/gnuplotinterface.hh>

#include "2pspatialparams.hh"

#ifndef ENABLEINTERFACESOLVER
#define ENABLEINTERFACESOLVER 0
#endif

namespace Dumux {
// forward declarations
template<class TypeTag> class TwoPTestProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct TwoPIncompressible { using InheritsFrom = std::tuple<TwoP>; };
struct TwoPIncompressibleTpfa { using InheritsFrom = std::tuple<TwoPIncompressible, CCTpfaModel>; };
struct TwoPIncompressibleMpfa { using InheritsFrom = std::tuple<TwoPIncompressible, CCMpfaModel>; };
struct TwoPIncompressibleBox { using InheritsFrom = std::tuple<TwoPIncompressible, BoxModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::TwoPIncompressible> { using type = Dune::ALUGrid</*dim=*/2, 2, Dune::cube, Dune::nonconforming>; };

// Set the problem type
template<class TypeTag>
struct Problem<TypeTag, TTag::TwoPIncompressible> { using type = TwoPTestProblem<TypeTag>; };

// the local residual containing the analytic derivative methods
// template<class TypeTag>
// struct LocalResidual<TypeTag, TTag::TwoPIncompressible> { using type = TwoPIncompressibleLocalResidual<TypeTag>; };

// Set the fluid system
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::TwoPIncompressible>
{
    
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
     using WettingPhase = FluidSystems::OnePLiquid<Scalar, Components::Brine<Scalar> >;
    using NonwettingPhase = FluidSystems::OnePGas<Scalar, Components::H2<Scalar> >;
    using type = FluidSystems::TwoPImmiscible<Scalar, WettingPhase, NonwettingPhase>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::TwoPIncompressible>
{
private:
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
public:
    using type = TwoPTestSpatialParams<FVGridGeometry, Scalar>;
};

// Enable caching
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::TwoPIncompressible> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::TwoPIncompressible> { static constexpr bool value = false; };
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::TwoPIncompressible> { static constexpr bool value = false; };

// Maybe enable the box-interface solver
template<class TypeTag>
struct EnableBoxInterfaceSolver<TypeTag, TTag::TwoPIncompressible> { static constexpr bool value = ENABLEINTERFACESOLVER; };
} // end namespace Properties

/*!
 * \ingroup TwoPTests
 * \brief The incompressible 2p test problem.
 */
template<class TypeTag>
class TwoPTestProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using Element = typename GridView::template Codim<0>::Entity;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using FVElementGeometry = typename FVGridGeometry::LocalView;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
     using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
     using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
     
    enum {
        pressureIdx = Indices::pressureIdx,
        saturationH2Idx = Indices::saturationIdx,
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::comp0Idx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::comp1Idx,
        nPhaseIdx = FluidSystem::phase0Idx,
        wPhaseIdx = FluidSystem::phase1Idx
        
    };

public:
    TwoPTestProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry) {}

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
   
  void setTime( Scalar time )
    {
        time_ = time;
    }

    void setTimeStepSize( Scalar timeStepSize )
     {
         std::cout << "timeStepSize " << timeStepSize;
        timeStepSize_ = timeStepSize;
     }
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {
        BoundaryTypes values;
        if (onLeftBoundary_(globalPos) || onRightBoundary_(globalPos))
            values.setAllDirichlet();
        else
            values.setAllNeumann();
        return values;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment.
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
       return initial_(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann boundary segment.
     *
     * \param globalPos The position of the integration point of the boundary segment.
     *
     * For this method, the \a values parameter stores the mass flux
     * in normal direction of each phase. Negative values mean influx.
     */
    NumEqVector neumannAtPos(const GlobalPosition &globalPos) const
    {
        NumEqVector values(0.0);

        return values;
    }
    
     NumEqVector source(const Element &element,
                       const FVElementGeometry& fvGeometry,
                       const ElementVolumeVariables& elemVolVars,
                       const SubControlVolume &scv) const
                       
    {const GlobalPosition& globalPos = element.geometry().center();
       
        NumEqVector values(0.0);
      Scalar cellHeight = 0.41;// für gebogenes Gitter
      Scalar effectiveWellRadius = 0.2*cellHeight;
      Scalar WellDiameter = 0.5335;
      Scalar permeability = 1.97e-12;
//       static Scalar mass=0.0;
      Scalar mobilityGas = std::max(0.1, elemVolVars[scv].mobility(nPhaseIdx));
      Scalar mobilityBrine = std::max(0.1, elemVolVars[scv].mobility(wPhaseIdx));
      Scalar WellIndex =(2.0*M_PI*cellHeight)/log(effectiveWellRadius/WellDiameter);
      Scalar InjectionPressure=1.53e7;
        Scalar ExtractionPressure= 1.4e5; 
      Scalar densityNw = std::max(elemVolVars[scv].density(nPhaseIdx), 60.0);
      Scalar densityW = std::max(elemVolVars[scv].density(wPhaseIdx), 800.0);

// 
        if (globalPos[0]> 49.5&& globalPos[0] <50.5  && globalPos[1] >20 && globalPos[1]<  20.5)
        { 
//             
//               std::cout << "mass1 " << mass;
             if(massInjected_<100 && massInjected_>0.0)
             {//values[contiNEqIdx] = 0.5;
               values[contiNEqIdx] = -WellIndex*permeability*mobilityGas*densityNw*(InjectionPressure - elemVolVars[scv].pressure(nPhaseIdx) )*(1.0/scv.geometry().volume());
                
//                 mass=values[contiNEqIdx]*scv.geometry().volume();
                      std::cout << "massinjection " << "   " << massInjected_ << std::endl;
                       std::cout << "pressure n " << elemVolVars[scv].pressure(nPhaseIdx);
                       std::cout << "pressurew " << elemVolVars[scv].pressure(wPhaseIdx)<< std::endl;
//                       std::cout << "timeStepSize_ " << timeStepSize_<<std::endl;
                   
             }

           else if(massInjected_>100 && time_<3000)
           {   values[contiNEqIdx]=0.0;
           std::cout << "pause" << massInjected_ <<std::endl;
           std::cout << "pressure n " << elemVolVars[scv].pressure(nPhaseIdx)<<std::endl;
            std::cout << "pressure w  " << elemVolVars[scv].pressure(wPhaseIdx);
           }
           else
           {  values[contiNEqIdx]=-0.105*(1.0/scv.geometry().volume());
//                values[contiNEqIdx] =- WellIndex*permeability*densityNw*mobilityGas*(ExtractionPressure -elemVolVars[scv].pressure(nPhaseIdx))*(1.0/scv.geometry().volume());
//                
//                 values[contiWEqIdx] = -WellIndex*permeability*densityW*mobilityBrine*(ExtractionPressure - elemVolVars[scv].pressure(nPhaseIdx))*(1.0/scv.geometry().volume()); 
                std::cout << "massextraction " << massInjected_<<std::endl;
                std::cout << "pressure n " << elemVolVars[scv].pressure(nPhaseIdx);
                 std::cout << "pressure w " << elemVolVars[scv].pressure(wPhaseIdx) << std::endl;
           }
        }
        
//         massInjected_ = mass;
/*    massInjected_+= values[contiNEqIdx]*scv.geometry().volume()*timeStepSize_; */ 
        return values;
    }


    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
    PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    {
       return initial_(globalPos);
    }

    /*!
     * \brief Returns the temperature \f$\mathrm{[K]}\f$ for an isothermal problem.
     *
     * This is not specific to the discretization. By default it just
     * throws an exception so it must be overloaded by the problem if
     * no energy equation is used.
     */
    Scalar temperature() const
    {
        return 313.15; // 40°C
    }
    
void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time)
    { 
        
//GNUPlot Ausgabe 
        Scalar yMax1;
        Scalar yMax2;
        static double yMax = -1e100;
      static std::vector<double> x;
      static std::vector<double> y;
      static std::vector<double> y2;
      static std::vector<double> y3;
      NumEqVector values;
      Scalar mass;
      Scalar actualmass=0.0;
    Scalar pressure;
    static Scalar oldTime;
    Scalar timeStepSize = time - oldTime;
//               
//               values = this->source(element, fvGeometry, elemVolVars[scv.insideScvIdx()], scv);
//               inflow += values[contiNEqIdx] * scv.geometry().volume();
/*      
      massInjected_ += -1.0 * values[contiNEqIdx] * scv.geometry().volume() * timeStepSize_;*/
      
            for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {const GlobalPosition& globalPos = element.geometry().center();
                
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);
                
                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);
                for (auto&& scv : scvs(fvGeometry))
                {   
                    if (globalPos[0]> 49.5&& globalPos[0] <50  && globalPos[1] >20 && globalPos[1]<  20.5)
                    {pressure= elemVolVars[scv].pressure(nPhaseIdx);
                    }
                    
                    if (globalPos[0]> 49.5&& globalPos[0] <50.5  && globalPos[1] >20 && globalPos[1]<  20.5)
                    {values = source(element, fvGeometry, elemVolVars, scv);
                    mass = (values[contiNEqIdx]/*+values[contiWEqIdx]*/) * scv.geometry().volume()*timeStepSize;
                    actualmass+=mass;
                    massInjected_+=mass;
                    }
                    if(mass > eps_ || mass< -eps_)
                    {
                    std::cout << "mass in postTimeStep " << actualmass << std::endl;
                     std::cout << "timeStepSize " << timeStepSize << std::endl;
                    }
                }
            }
            
     oldTime = time;
     

      Scalar time_plot = time_/3600.0;
      Scalar massInjected_plot = massInjected_;
      Scalar mass_plot= actualmass;
       Scalar massExtracted_plot = massExtracted_;
     Scalar pressure_plot=pressure;
      
      x.push_back(time_plot);
      y.push_back(massInjected_plot);
      y2.push_back(mass_plot);
      y3.push_back(pressure_plot);
      
      yMax1 = std::max({yMax, massInjected_plot});
      yMax2 = std::max({yMax, massExtracted_plot});
      
      gnuplot_.resetPlot();
      gnuplot_.setXRange(0, x.back()+1);
      gnuplot_.setYRange(0.0, yMax1+1);
      gnuplot_.setXlabel("Zeit [h]");
      gnuplot_.setYlabel("Injected mass");
      gnuplot_.addDataSetToPlot(x, y, "Injected mass", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y2, "actual mass", "axes x1y1 w l");
      gnuplot_.plot("Injected_mass");
      
      gnuplot2_.resetPlot();
      gnuplot2_.setXRange(0, x.back()+1);
      gnuplot2_.setYRange(0.0, 1.6e7);
      gnuplot2_.setXlabel("Zeit [h]");
      gnuplot2_.setYlabel("pressure");
      gnuplot2_.addDataSetToPlot(x, y3, "pressure", "axes x1y1 w l");
      gnuplot2_.plot("pressure");
    }
private:
      PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
    PrimaryVariables priVars(0.0);
//     priVars.setState(wPhaseOnly);
        
       Scalar densityW = 1000.0;
//       Scalar densityNw = 0.81;
      priVars[pressureIdx] = 1e7+ (maxDepth_ - globalPos[1])*densityW*9.81;

    priVars[saturationH2Idx] = 0.0;
    
    return priVars;
     }
    Scalar maxdepth_;
    bool onLeftBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] < this->fvGridGeometry().bBoxMin()[0] + eps_;
    }

    bool onRightBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[0] > this->fvGridGeometry().bBoxMax()[0] - eps_;
    }

    bool onLowerBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] < this->fvGridGeometry().bBoxMin()[1] + eps_;
    }

    bool onUpperBoundary_(const GlobalPosition &globalPos) const
    {
        return globalPos[1] > this->fvGridGeometry().bBoxMax()[1] - eps_;
    }

    bool onInlet_(const GlobalPosition &globalPos) const
    {
        Scalar width = this->fvGridGeometry().bBoxMax()[0] - this->fvGridGeometry().bBoxMin()[0];
        Scalar lambda = (this->fvGridGeometry().bBoxMax()[0] - globalPos[0])/width;
        return onUpperBoundary_(globalPos) && 0.5 < lambda && lambda < 2.0/3.0;
    }
    

    static constexpr Scalar eps_ = 1e-6;
       Scalar maxDepth_;
    std::string name_;
    Scalar massExtracted_;
    Scalar mutable injectionPressure_;
    Scalar mutable massInjected_;
    Scalar massInjectedTimestep_;
    Scalar inflow;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    bool mutable isExtracting_;
    bool mutable reachedLowerBound_;
    Scalar time_;
    Scalar timeStepSize_;
};

} // end namespace Dumux

#endif
