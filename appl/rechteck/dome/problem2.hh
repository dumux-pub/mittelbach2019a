// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 */

#ifndef DUMUX_INJECTION_PROBLEM_HH
#define DUMUX_INJECTION_PROBLEM_HH

#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_DUNE_ALUGRID, HAVE_UG

#include <dumux/io/gnuplotinterface.hh>

#include <dumux/discretization/cellcentered/mpfa/omethod/staticinteractionvolume.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p2c/model.hh>
#include <dumux/material/fluidsystems/brineh2.hh>


#include "spatialparams.hh"

namespace Dumux {

#ifndef ENABLECACHING
#define ENABLECACHING 0
#endif

template <class TypeTag>
class InjectionProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct Injection { using InheritsFrom = std::tuple<TwoPTwoC>; };
struct InjectionBox { using InheritsFrom = std::tuple<Injection, BoxModel>; };
struct InjectionCCTpfa { using InheritsFrom = std::tuple<Injection, CCTpfaModel>; };
struct InjectionCCMpfa { using InheritsFrom = std::tuple<Injection, CCMpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Injection> { using type = Dune::/*YaspGrid<2*/ALUGrid<2, 2, Dune::cube, Dune::nonconforming>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Injection> { using type = InjectionProblem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Injection>
{
     using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::BrineH2<Scalar, Components::Brine<Scalar>>;
//     using type = FluidSystems::BrineH2<GetPropType<TypeTag, Properties::Scalar>,
//                                      FluidSystems::BrineH2DefaultPolicy</*fastButSimplifiedRelations=*/true>>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Injection>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = InjectionSpatialParams<FVGridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::Injection> { static constexpr bool value = false; };

// Enable caching or not (reference solutions created without caching)
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };

// use the static interaction volume around interior vertices in the mpfa test
template<class TypeTag>
struct PrimaryInteractionVolume<TypeTag, TTag::InjectionCCMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;

    // structured two-d grid
    static constexpr int numIvScvs = 4;
    static constexpr int numIvScvfs = 4;

    // use the default traits
    using Traits = CCMpfaODefaultStaticInteractionVolumeTraits< NodalIndexSet, Scalar, numIvScvs, numIvScvfs >;
public:
    using type = CCMpfaOStaticInteractionVolume< Traits >;
};
} // end namespace Properties

/*!
 * \ingroup TwoPTwoCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 *
 * The domain is sized 60m times 40m and consists of two layers, a moderately
 * permeable one (\f$ K=10e-12\f$) for \f$ y<22m\f$ and one with a lower
 * permeablility (\f$ K=10e-13\f$) in the rest of the domain.
 *
 * A mixture of Nitrogen and Water vapor, which is composed according to the
 * prevailing conditions (temperature, pressure) enters a water-filled aquifer.
 * This is realized with a solution-dependent Neumann boundary condition at the
 * right boundary (\f$ 5m<y<15m\f$). The aquifer is situated 2700m below sea level.
 * The injected fluid phase migrates upwards due to buoyancy.
 * It accumulates and partially enters the lower permeable aquitard.
 *
 * The model is able to use either mole or mass fractions. The property useMoles
 * can be set to either true or false in the problem file. Make sure that the
 * according units are used in the problem set-up.
 * The default setting for useMoles is true.
 *
 * This problem uses the \ref TwoPTwoCModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p2c</tt> or
 * <tt>./test_cc2p2c</tt>
 */
template <class TypeTag>
class InjectionProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
//     using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;



    // equation indices
    enum
    {
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::BrineIdx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::H2Idx,
    };

    // phase indices
    enum
    {
        gasPhaseIdx = FluidSystem::H2Idx,
        BrineIdx = FluidSystem::BrineIdx,
        H2Idx = FluidSystem::H2Idx
    };
    
      enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wCompIdx = FluidSystem::BrineIdx,
        nCompIdx = FluidSystem::H2Idx,

        wPhaseIdx = FluidSystem::BrineIdx,
        nPhaseIdx = FluidSystem::H2Idx,

        // Phase State
        wPhaseOnly = Indices::firstPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,

    };
    


    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;
       
    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = ModelTraits::useMoles();

public:
    InjectionProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
         nTemperature_       = getParam<int>("Problem.NTemperature");
        nPressure_          = getParam<int>("Problem.NPressure");
        pressureLow_        = getParam<Scalar>("Problem.PressureLow");
        pressureHigh_       = getParam<Scalar>("Problem.PressureHigh");
        temperatureLow_     = getParam<Scalar>("Problem.TemperatureLow");
        temperatureHigh_    = getParam<Scalar>("Problem.TemperatureHigh");
        temperature_        = getParam<Scalar>("Problem.InitialTemperature");
        depthBOR_           = getParam<Scalar>("Problem.DepthBOR");
        name_               = getParam<std::string>("Problem.Name");


//         stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole-fractions"<<std::endl;
        else
            std::cout<<"problem uses mass-fractions"<<std::endl;
    }

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K]
     */
    Scalar temperature() const
    { return temperature_; }

    // \}
    void setTimeStepSize( Scalar timeStepSize )
     {
        timeStepSize_ = timeStepSize;
     }
     
      void setTime( Scalar time )
    {
        time_ = time;
    }


    /*!
     * \name Boundary conditions
     */
    // \{

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {  
        BoundaryTypes bcTypes;
        Scalar right = this->fvGridGeometry().bBoxMax()[0];

        if(globalPos[0] > right - eps_ || globalPos[0] < eps_)//Dirichlet rechts und links für den Druck
            bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();
        
   
        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.Dirichlet
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        
        return values;
    }

    
    //! \copydoc Dumux::FVProblem::source()
    NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scv.dofPosition();


      Scalar cellHeight = 0.41;// für gebogenes Gitter
      Scalar effectiveWellRadius = 0.2*cellHeight;
      Scalar WellDiameter = 0.5335;
      Scalar permeability = this->spatialParams().permeabilityAtPos(globalPos);

      Scalar mobilityGas = std::max(0.1, elemVolVars[scv].mobility(nPhaseIdx));
      Scalar mobilityBrine = std::max(0.1, elemVolVars[scv].mobility(wPhaseIdx));
      Scalar WellIndex =(2.0*M_PI*cellHeight)/log(effectiveWellRadius/WellDiameter);
//       Scalar InjectionPressure=1.53e7;

      Scalar densityNw = std::max(elemVolVars[scv].density(nPhaseIdx), 60.0);
      Scalar densityW = std::max(elemVolVars[scv].density(wPhaseIdx), 800.0);
     
//         Scalar injectionTemperature = 322;
//     Scalar ExtractionPressure=0.95e5;
    
//     if (globalPos[0]> 48&& globalPos[0] <52 && globalPos[1]>5&& globalPos[1]<8)
//     {
//         if (massInjected_<5000)
//         values[contiNEqIdx]=0.5;
//         else
//         values[contiNEqIdx]=0.0;   
//     }

    
        
       if (globalPos[0]> 49.5&& globalPos[0] <50.5 && globalPos[1]>20&& globalPos[1]<20.5)
        {      
 

            if(massInjected_<50)
            {injectionPressure_=1.53e7;
               values[contiNEqIdx] = WellIndex*permeability*mobilityGas*densityNw*(elemVolVars[scv].pressure(nPhaseIdx) - injectionPressure_)*(1.0/scv.geometry().volume());
                
//                 mass+=values[contiNEqIdx]*scv.geometry().volume()*timeStepSize_;
//                       std::cout << "massinjection " << massInjected_<< "pressure  " << elemVolVars[scv].pressure(nPhaseIdx)<<std::endl;
            }

           else if( massInjected_>50 && time_<200)
           {   values[contiNEqIdx]=0.0;
//            std::cout << "pause" << massInjected_ <<std::endl;
               
        }
           else
           {  values[contiNEqIdx]=-0.105*(1.0/scv.geometry().volume());}
//                injectionPressure_= (values[contiNEqIdx]*(scv.geometry().volume())+elemVolVars[scv].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                      *elemVolVars[scv].pressure(nPhaseIdx)+elemVolVars[scv].massFraction(wPhaseIdx,nCompIdx)*mobilityBrine*
//                              WellIndex*densityW*permeability*elemVolVars[scv].pressure(wPhaseIdx))*(1.0/(elemVolVars[scv].massFraction(nPhaseIdx,nCompIdx)*mobilityGas*WellIndex*densityNw*permeability
//                   +elemVolVars[scv].massFraction(wPhaseIdx,nCompIdx)*mobilityBrine*WellIndex*densityW*permeability));

// std::cout << "massextraction " << massInjected_<< "pressure" << elemVolVars[scv].pressure(nPhaseIdx)<< std::endl;
                
//                 values[contiNEqIdx]=- (elemVolVars[scv].massFraction(nPhaseIdx,nCompIdx) *mobilityGas*WellIndex*densityNw*permeability *(ExtractionPressure-elemVolVars[scv].pressure(nPhaseIdx)) - elemVolVars[scv].massFraction(wPhaseIdx,nCompIdx)*mobilityBrine*
//                             WellIndex*densityW*permeability*(ExtractionPressure-elemVolVars[scv].pressure(wPhaseIdx)))*(1.0/scv.geometry().volume());
//                             
//                 values[contiWEqIdx]=-(elemVolVars[scv].massFraction(nPhaseIdx,wCompIdx)*mobilityGas *WellIndex*densityNw*permeability *(ExtractionPressure-elemVolVars[scv].pressure(nPhaseIdx)) - elemVolVars[scv].massFraction(wPhaseIdx,wCompIdx)*mobilityBrine*
//                             WellIndex*densityW*permeability*(ExtractionPressure-elemVolVars[scv].pressure(wPhaseIdx)))*(1.0/scv.geometry().volume());
           
        }
           
        
        return values;
    }
    // \}

    /*!
     * \name Volume terms
     */
    // \{

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }


  void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time,
                      const Scalar timeStepIndex)
                   
    {   NumEqVector values;
      Scalar mass;
      Scalar actualmass=0.0;
    Scalar pressure;
    static Scalar oldTime;
    Scalar timeStepSize = time - oldTime;
    Scalar ConvMass=0.0;
    Scalar Volume=0.0;
    Scalar F=0.0;


      
    for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {const GlobalPosition& globalPos = element.geometry().center();
                
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);
                
                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);
                for (auto&& scv : scvs(fvGeometry))
                {   
                   
                    
                    if (globalPos[0]> 49.5&& globalPos[0] <50.5  && globalPos[1] >20 && globalPos[1]<  20.5)
                    {values = source(element, fvGeometry, elemVolVars, scv);
                    mass = (values[contiNEqIdx]/*+values[contiWEqIdx]*/) * scv.geometry().volume()*timeStepSize;
                
                    massInjected_+=mass;
                    }
                    
                const auto& volVars = elemVolVars[scv];
                ConvMass += volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(nPhaseIdx)
                              * scv.volume() * volVars.saturation(wPhaseIdx)/timeStepSize;
                Volume+= scv.volume();
                F=ConvMass/Volume/time;
/*
                accumulatedSource_ += this->scvPointSources(element, fvGeometry, elemVolVars, scv)[compIdx]
                                       * scv.volume() * volVars.extrusionFactor()
                                       * FluidSystem::molarMass(compIdx)
                                       * timeStepSize;*/
                }
            }
            oldTime = time;
            std::cout << " massinjected   " << massInjected_<< std::endl;
            std::cout << " convmass   " << ConvMass<< std::endl;
             std::cout << " volume   " << Volume<< std::endl;
             std::cout << " F   " << F<< std::endl;

               
      

     
        
        
// //GNUPlot Ausgabe 
        Scalar yMax1;
        Scalar yMax2;
        static double yMax = -1e100;
      static std::vector<double> x;
      static std::vector<double> y;
      static std::vector<double> y2;
      static std::vector<double> y3;
      static std::vector<double> y4;
        
// 
//               
//               values = this->source(element, fvGeometry, elemVolVars[scv.insideScvIdx()], scv);
//               inflow += values[contiNEqIdx] * scv.geometry().volume();
/*      
      massInjected_ += -1.0 * values[contiNEqIdx] * scv.geometry().volume() * timeStepSize_;*/
            
          for (const auto& element : elements(this->fvGridGeometry().gridView()))
            {const GlobalPosition& globalPos = element.geometry().center();
                
                auto fvGeometry = localView(this->fvGridGeometry());
                fvGeometry.bindElement(element);
                
                auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);
                for (auto&& scv : scvs(fvGeometry))
                {   
                    if (globalPos[0]> 49.5&& globalPos[0] <50  && globalPos[1] >20 && globalPos[1]<  20.5)
                    {pressure= elemVolVars[scv].pressure(nPhaseIdx);
                    }
                    
                    if (globalPos[0]> 49.5&& globalPos[0] <50.5  && globalPos[1] >20 && globalPos[1]<  20.5)
                    {values = source(element, fvGeometry, elemVolVars, scv);
                    mass = (values[contiNEqIdx]/*+values[contiWEqIdx]*/) * scv.geometry().volume()*timeStepSize;
                    actualmass+=mass;
                    massInjected_+=mass;
                    }
                    if(mass > eps_ || mass< -eps_)
                    {
                    std::cout << "mass in postTimeStep " << actualmass << std::endl;
                     std::cout << "timeStepSize " << timeStepSize << std::endl;
                    }
                }
            }

   oldTime = time;
//      
// 
      Scalar time_plot = time_/3600.0;
      Scalar massInjected_plot = massInjected_;
      Scalar mass_plot= actualmass;
       Scalar massExtracted_plot = massExtracted_;
     Scalar pressure_plot=pressure;
     Scalar wellpressure_plot= injectionPressure_;
      
      x.push_back(time_plot);
      y.push_back(massInjected_plot);
      y2.push_back(mass_plot);
      y3.push_back(pressure_plot);
      y4.push_back(wellpressure_plot);
      
      yMax1 = std::max({yMax, massInjected_plot});
      yMax2 = std::max({yMax, wellpressure_plot});
      
      gnuplot_.resetPlot();
      gnuplot_.setXRange(0, x.back()+1);
      gnuplot_.setYRange(0.0, yMax1+1);
      gnuplot_.setXlabel("Zeit [h]");
      gnuplot_.setYlabel("Injected mass");
      gnuplot_.addDataSetToPlot(x, y, "Injected mass", "axes x1y1 w l");
      gnuplot_.addDataSetToPlot(x, y2, "actual mass", "axes x1y1 w l");
      gnuplot_.plot("Injected_mass");
      
      gnuplot2_.resetPlot();
      gnuplot2_.setXRange(0, x.back()+1);
      gnuplot2_.setYRange(0.0, 1.6e7);
      gnuplot2_.setXlabel("Zeit [h]");
      gnuplot2_.setYlabel("pressure");
      gnuplot2_.addDataSetToPlot(x, y3, "pressure n", "axes x1y1 w l");
       gnuplot2_.addDataSetToPlot(x, y4, "wellpressure", "axes x1y1 w l");
      gnuplot2_.plot("pressure");
    }
    

private:
    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * The internal method for the initial condition
     *
     * \param globalPos The global position
     */
    
PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
    PrimaryVariables priVars(0.0);
    priVars.setState(wPhaseOnly);
        
       Scalar densityW = 1000.0;
//       Scalar densityNw = 0.81;
      priVars[pressureIdx] = 1e7 + (maxDepth_ - globalPos[1])*densityW*9.81;
    priVars[switchIdx] = 0.0;


    return priVars;
     }
    Scalar inflow;
    Scalar maxDepth_;
    std::string name_;
    Scalar massInjected_;
    Scalar massExtracted_;
    Scalar mutable injectionPressure_;
    Scalar energyInjected_;
    Scalar massInjectedTimestep_;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    bool mutable isExtracting_;
    bool mutable reachedLowerBound_;
    Scalar time_;
    Scalar timeStepSize_;

    Scalar temperature_;
    Scalar depthBOR_;
    static constexpr Scalar eps_ = 1e-6;

    int nTemperature_;
    int nPressure_;
    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;

};



} // end namespace Dumux

#endif
