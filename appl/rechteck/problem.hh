// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup TwoPTwoCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 500m.
 */

#ifndef DUMUX_INJECTION_PROBLEM_HH
#define DUMUX_INJECTION_PROBLEM_HH

#if  HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#elif HAVE_UG
#include <dune/grid/uggrid.hh>
#else
#include <dune/grid/yaspgrid.hh>
#endif // HAVE_DUNE_ALUGRID, HAVE_UG

#include <dumux/io/gnuplotinterface.hh>

#include <dumux/discretization/cellcentered/mpfa/omethod/staticinteractionvolume.hh>
#include <dumux/discretization/ccmpfa.hh>
#include <dumux/discretization/cctpfa.hh>
#include <dumux/discretization/box.hh>

#include <dumux/porousmediumflow/problem.hh>
#include <dumux/porousmediumflow/2p2c/model.hh>
#include <appl/material/fluidsystems/brineh2.hh>
#include <dumux/material/fluidsystems/brineco2.hh>

#include <dumux/material/constraintsolvers/compositionalflash.hh>
#include <dumux/material/fluidmatrixinteractions/mp/mplinearmaterial.hh>


#include "spatialparams.hh"

namespace Dumux {

#ifndef ENABLECACHING
#define ENABLECACHING 0
#endif

template <class TypeTag>
class InjectionProblem;

namespace Properties {
// Create new type tags
namespace TTag {
struct Injection { using InheritsFrom = std::tuple<TwoPTwoC>; };
struct InjectionBox { using InheritsFrom = std::tuple<Injection, BoxModel>; };
struct InjectionCCTpfa { using InheritsFrom = std::tuple<Injection, CCTpfaModel>; };
struct InjectionCCMpfa { using InheritsFrom = std::tuple<Injection, CCMpfaModel>; };
} // end namespace TTag

// Set the grid type
template<class TypeTag>
struct Grid<TypeTag, TTag::Injection> { using type = Dune::UGGrid<2>; };

// Set the problem property
template<class TypeTag>
struct Problem<TypeTag, TTag::Injection> { using type = InjectionProblem<TypeTag>; };

// Set fluid configuration
template<class TypeTag>
struct FluidSystem<TypeTag, TTag::Injection>
{
     using Scalar = typename GET_PROP_TYPE(TypeTag, Scalar);
    using type = FluidSystems::BrineH2<Scalar, Components::Brine<Scalar>>;
//     using type = FluidSystems::BrineH2<GetPropType<TypeTag, Properties::Scalar>,
//                                      FluidSystems::BrineH2DefaultPolicy</*fastButSimplifiedRelations=*/true>>;
    using CompositionalFluidState = Dumux::CompositionalFluidState<Scalar, FluidSystem>;
};

// Set the spatial parameters
template<class TypeTag>
struct SpatialParams<TypeTag, TTag::Injection>
{
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using type = InjectionSpatialParams<FVGridGeometry, Scalar>;
};

// Define whether mole(true) or mass (false) fractions are used
template<class TypeTag>
struct UseMoles<TypeTag, TTag::Injection> { static constexpr bool value = false; };

// Enable caching or not (reference solutions created without caching)
template<class TypeTag>
struct EnableFVGridGeometryCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridVolumeVariablesCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };
template<class TypeTag>
struct EnableGridFluxVariablesCache<TypeTag, TTag::Injection> { static constexpr bool value = ENABLECACHING; };

// use the static interaction volume around interior vertices in the mpfa test
template<class TypeTag>
struct PrimaryInteractionVolume<TypeTag, TTag::InjectionCCMpfa>
{
private:
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using NodalIndexSet = GetPropType<TypeTag, Properties::DualGridNodalIndexSet>;

    // structured two-d grid
    static constexpr int numIvScvs = 4;
    static constexpr int numIvScvfs = 4;

    // use the default traits
    using Traits = CCMpfaODefaultStaticInteractionVolumeTraits< NodalIndexSet, Scalar, numIvScvs, numIvScvfs >;
public:
    using type = CCMpfaOStaticInteractionVolume< Traits >;
};
} // end namespace Properties

/*!
 * \ingroup TwoPTwoCTests
 * \brief Problem where air is injected under a low permeable layer in a depth of 2700m.
 *
 * The domain is sized 60m times 40m and consists of two layers, a moderately
 * permeable one (\f$ K=10e-12\f$) for \f$ y<22m\f$ and one with a lower
 * permeablility (\f$ K=10e-13\f$) in the rest of the domain.
 *
 * A mixture of Nitrogen and Water vapor, which is composed according to the
 * prevailing conditions (temperature, pressure) enters a water-filled aquifer.
 * This is realized with a solution-dependent Neumann boundary condition at the
 * right boundary (\f$ 5m<y<15m\f$). The aquifer is situated 2700m below sea level.
 * The injected fluid phase migrates upwards due to buoyancy.
 * It accumulates and partially enters the lower permeable aquitard.
 *
 * The model is able to use either mole or mass fractions. The property useMoles
 * can be set to either true or false in the problem file. Make sure that the
 * according units are used in the problem set-up.
 * The default setting for useMoles is true.
 *
 * This problem uses the \ref TwoPTwoCModel.
 *
 * To run the simulation execute the following line in shell:
 * <tt>./test_box2p2c</tt> or
 * <tt>./test_cc2p2c</tt>
 */
template <class TypeTag>
class InjectionProblem : public PorousMediumFlowProblem<TypeTag>
{
    using ParentType = PorousMediumFlowProblem<TypeTag>;
    using GridView = GetPropType<TypeTag, Properties::GridView>;
    using FVGridGeometry = GetPropType<TypeTag, Properties::FVGridGeometry>;
    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using FluidState = typename GET_PROP_TYPE(TypeTag, FluidState);
    using FluidSystem = GetPropType<TypeTag, Properties::FluidSystem>;
    using WettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::Brine;
    using NonWettingPhase = typename GetPropType<TypeTag, Properties::FluidSystem>::H2;
//     using TimeManager = typename GET_PROP_TYPE(TypeTag, TimeManager);
    
    using ModelTraits = GetPropType<TypeTag, Properties::ModelTraits>;
    using Indices = typename ModelTraits::Indices;



    // equation indices
    enum
    {
        contiWEqIdx = Indices::conti0EqIdx + FluidSystem::BrineIdx,
        contiNEqIdx = Indices::conti0EqIdx + FluidSystem::H2Idx,
    };

    // phase indices
    enum
    {
        gasPhaseIdx = FluidSystem::H2Idx,
        BrineIdx = FluidSystem::BrineIdx,
        H2Idx = FluidSystem::H2Idx
    };
    
      enum {
        pressureIdx = Indices::pressureIdx,
        switchIdx = Indices::switchIdx,

        wCompIdx = FluidSystem::BrineIdx,
        nCompIdx = FluidSystem::H2Idx,

        wPhaseIdx = FluidSystem::BrineIdx,
        nPhaseIdx = FluidSystem::H2Idx,

        // Phase State
        wPhaseOnly = Indices::firstPhaseOnly,
        bothPhases = Indices::bothPhases,

        // Grid and world dimension
        dim = GridView::dimension,
        dimWorld = GridView::dimensionworld,
        numPhases = FluidSystem::numPhases,

    };
    


    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using NumEqVector = GetPropType<TypeTag, Properties::NumEqVector>;
    using ElementVolumeVariables = typename GetPropType<TypeTag, Properties::GridVolumeVariables>::LocalView;
    using BoundaryTypes = GetPropType<TypeTag, Properties::BoundaryTypes>;
    using Element = typename GridView::template Codim<0>::Entity;
    using GlobalPosition = typename Element::Geometry::GlobalCoordinate;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::FVGridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using GridVariables = GetPropType<TypeTag, Properties::GridVariables>;

    using MaterialLaw = Dumux::MpLinearMaterial<numPhases, Scalar>;
    using MaterialLawParams = typename MaterialLaw::Params;

    using CellArray = std::array<unsigned int, dimWorld>;
    static constexpr bool isBox = FVGridGeometry::discMethod == DiscretizationMethod::box;
    enum { dofCodim = isBox ? dim : 0 };
    using PhaseVector = Dune::FieldVector<Scalar, numPhases>;
    //! Property that defines whether mole or mass fractions are used
    static constexpr bool useMoles = ModelTraits::useMoles();

public:
    using SpatialParams = typename GET_PROP_TYPE(TypeTag, SpatialParams) ;
    using Materiallaw = typename SpatialParams::MaterialLaw;
    InjectionProblem(std::shared_ptr<const FVGridGeometry> fvGridGeometry)
    : ParentType(fvGridGeometry)
    {
         nTemperature_       = getParam<int>("Problem.NTemperature");
        nPressure_          = getParam<int>("Problem.NPressure");
        pressureLow_        = getParam<Scalar>("Problem.PressureLow");
        pressureHigh_       = getParam<Scalar>("Problem.PressureHigh");
        temperatureLow_     = getParam<Scalar>("Problem.TemperatureLow");
        temperatureHigh_    = getParam<Scalar>("Problem.TemperatureHigh");
        temperature_        = getParam<Scalar>("Problem.InitialTemperature");
        depthBOR_           = getParam<Scalar>("Problem.DepthBOR");
        name_               = getParam<std::string>("Problem.Name");
        maxDepth_           =500.0;

        //stating in the console whether mole or mass fractions are used
        if(useMoles)
            std::cout<<"problem uses mole-fractions"<<std::endl;
        else
            std::cout<<"problem uses mass-fractions"<<std::endl;
        
        // store pointer to all elements in a multimap, elements belonging to the same column
        // have the same key, starting with key = 0 for first column
        // Note: only works for equidistant grids
        int columnIndex = 0;
        for (const auto& element : Dune::elements(this->fvGridGeometry().gridView()))
        {
            // identify column number
            GlobalPosition globalPos = element.geometry().center();
            CellArray numberOfCellsX = getParam<CellArray>("Grid.Cells");
            double deltaX = this->fvGridGeometry().bBoxMax()[0]/numberOfCellsX[0];

            columnIndex = round((globalPos[0] + (deltaX/2.0))/deltaX);

            mapColumns_.insert(std::make_pair(columnIndex, element));
            dummy_ = element;
        }

        //calculate length of capillary transition zone (CTZ)
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar swr = this->spatialParams().materialLawParamsAtPos(globalPos).swr();
        Scalar snr = this->spatialParams().materialLawParamsAtPos(globalPos).snr();
        Scalar satW1 = 1.0 - snr;
        Scalar satW2 = swr + 0.1*(1.0-swr-snr);
        Scalar pc1 = Materiallaw::pc(this->spatialParams().materialLawParamsAtPos(globalPos), satW1);
        Scalar pc2 = Materiallaw::pc(this->spatialParams().materialLawParamsAtPos(globalPos), satW2);

        Scalar pRef = pressure(globalPos);
        Scalar tempRef = temperature();
        Scalar densityW = WettingPhase::liquidDensity(tempRef, pRef);
        Scalar densityN = NonWettingPhase::gasDensity(tempRef, pRef);
        Scalar gravity = this->gravity().two_norm();
        CTZ_ = (pc2-pc1)/((densityW-densityN)*gravity);
        std::cout << "CTZ " << CTZ_ << std::endl;

        //calculate segregation time
        Scalar height = this->fvGridGeometry().bBoxMax()[dim-1];
        Scalar porosity = this->spatialParams().porosityAtPos(globalPos);
        viscosityW_ = WettingPhase::liquidViscosity(tempRef, pRef);
        Scalar permeability = this->spatialParams().permeabilityAtPos(globalPos);
        segTime_ = (height*porosity*viscosityW_*(1-swr))/(permeability*gravity*(densityW-densityN));
        std::cout << "segTime " << segTime_ << std::endl;

        veModel_ = getParam<int>("VE.VEModel");

        system("rm *.out");
    }
    
    

    /*!
     * \name Problem parameters
     */
    // \{

    /*!
     * \brief Returns the problem name
     *
     * This is used as a prefix for files generated by the simulation.
     */
    const std::string& name() const
    { return name_; }

    /*!
     * \brief Returns the temperature [K]
     */
    Scalar temperature() const
    { return temperature_; }
    
    //returns hydrostatic pressure
    Scalar pressure(const GlobalPosition &globalPos) const
    {
        Scalar p=1e7-9.81*1014*globalPos[1];
//         Scalar p=1e7;
        return p; }

    /*!
     * \brief Specifies which kind of boundary condition should be
     *        used for which equation on a given boundary segment
     *
     * \param globalPos The global position
     */
    BoundaryTypes boundaryTypesAtPos(const GlobalPosition &globalPos) const
    {  
        BoundaryTypes bcTypes;
        Scalar right = this->fvGridGeometry().bBoxMax()[0];

        if(globalPos[0] > right - eps_ /*|| globalPos[0] < eps_*/)//Dirichlet rechts für den Druck
            bcTypes.setAllDirichlet();
        else
            bcTypes.setAllNeumann();
        
   
        return bcTypes;
    }

    /*!
     * \brief Evaluates the boundary conditions for a Dirichlet boundary segment
     *
     * \param globalPos The global position
     */
    PrimaryVariables dirichletAtPos(const GlobalPosition &globalPos) const
    {
        return initial_(globalPos);
    }

    /*!
     * \brief Evaluates the boundary conditions for a Neumann
     *        boundary segment in dependency on the current solution.Dirichlet
     *
     * \param element The finite element
     * \param fvGeometry The finite volume geometry of the element
     * \param elemVolVars All volume variables for the element
     * \param scvf The sub-control volume face
     *
     * This method is used for cases, when the Neumann condition depends on the
     * solution and requires some quantities that are specific to the fully-implicit method.
     * The \a values store the mass flux of each phase normal to the boundary.
     * Negative values indicate an inflow.
     */
    NumEqVector neumann(const Element& element,
                        const FVElementGeometry& fvGeometry,
                        const ElementVolumeVariables& elemVolVars,
                        const SubControlVolumeFace& scvf) const
    {
        NumEqVector values(0.0);
        return values;
    }

    
    //! \copydoc Dumux::FVProblem::source()
NumEqVector source(const Element &element,
                   const FVElementGeometry& fvGeometry,
                   const ElementVolumeVariables& elemVolVars,
                   const SubControlVolume &scv) const
    {
        NumEqVector values(0.0);
        const auto& globalPos = scv.dofPosition();

       if (globalPos[0]> eps_&& globalPos[0] <3) // line source on the left side
        {
               values[contiNEqIdx] =0.00005;
        }
        return values;
    }

    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * \param globalPos The global position
     */
PrimaryVariables initialAtPos(const GlobalPosition &globalPos) const
    { return initial_(globalPos); }

//add fields to paraview output
template<class VTKWriter>
void addVtkFields(VTKWriter& vtk)
    {
        const auto& gridView = this->fvGridGeometry().gridView();
        Kxx_.resize(gridView.size(dofCodim));
        Kyy_.resize(gridView.size(dofCodim));
        D_h2_.resize(gridView.size(dofCodim));
        C_H2_.resize(gridView.size(dofCodim));
        mu_Brine_.resize(gridView.size(dofCodim));

        vtk.addField(Kxx_, "Knw");
        vtk.addField(Kyy_, "Kw");
        vtk.addField(D_h2_, "D_H2");
        vtk.addField(mu_Brine_, "mu_liq");
        vtk.addField(C_H2_, "C_H2");
    }
    
void updateVtkFields(const SolutionVector& curSol)
    {
        for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {
            auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());

            auto fvGeometry = localView(this->fvGridGeometry());
            fvGeometry.bindElement(element);

            for (auto&& scv : scvs(fvGeometry))
            {
                VolumeVariables volVars;
                volVars.update(elemSol, *this, element, scv);
                const auto dofIdxGlobal = scv.dofIndex();

                Kxx_[dofIdxGlobal] = volVars.relativePermeability(nPhaseIdx);
                Kyy_[dofIdxGlobal] = volVars.relativePermeability(wPhaseIdx);
                D_h2_[dofIdxGlobal] = volVars.diffusionCoefficient(wPhaseIdx,nCompIdx);
                mu_Brine_[dofIdxGlobal] = volVars.viscosity(wPhaseIdx);
                C_H2_[dofIdxGlobal] = volVars.massFraction(nPhaseIdx,wCompIdx)*volVars.density(nPhaseIdx);
            }
        }
    }
    
void postTimeStep(const SolutionVector& curSol,
                      const GridVariables& gridVariables,
                      const Scalar time,
                      const Scalar timeStepSize,
                      const Scalar timeStepIndex)
    {
        const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");

    NumEqVector values;
    Scalar mass;
    Scalar ConvMass=0.0;
    Scalar convMass=0.0;

    for (const auto& element : elements(this->fvGridGeometry().gridView()))
        {const GlobalPosition& globalPos = element.geometry().center();
            Scalar porosity = this->spatialParams().porosityAtPos(globalPos);

         auto fvGeometry = localView(this->fvGridGeometry());
              fvGeometry.bindElement(element);

         auto elemVolVars = localView(gridVariables.curGridVolVars());
                elemVolVars.bindElement(element, fvGeometry, curSol);

            for (auto&& scv : scvs(fvGeometry))
                {
                    if (globalPos[0]> eps_&& globalPos[0] <3)
                    {values = source(element, fvGeometry, elemVolVars, scv);
                    mass = (values[contiNEqIdx]) * scv.geometry().volume()*timeStepSize;

                    massInjected_+=mass;                    //accumulates injected mass

                    }
                const auto& volVars = elemVolVars[scv];
            auto elemSol = elementSolution(element, curSol, this->fvGridGeometry());
            const auto& priVars = elemSol[scv.localDofIndex()];
            const auto phasePresence = priVars.state();

            if(phasePresence==1)
                    {
                    ConvMass += volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wPhaseIdx)
                              * scv.volume() * volVars.saturation(wPhaseIdx)*porosity;      //mass dissolved beyond the plume

                    }
                    convMass+=volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wPhaseIdx)
                    * scv.volume() * volVars.saturation(wPhaseIdx)*porosity;            //mass dissolved complete
                }
            }

            std::cout << " massinjected   " << massInjected_<< std::endl;
            std::cout << " convmass   " << ConvMass<< std::endl;
             
      Scalar time_plot = time/3600.0;
      Scalar massInjected_plot = massInjected_;
      Scalar prozent1= 100/massInjected_*ConvMass;
      Scalar prozent2= 100/massInjected_*convMass;

      std::ostringstream oss;
      oss << "Masse_.out";
      std::string fileName = oss.str();
      outputFile_.open(fileName, std::ios::app);
      outputFile_ << time_plot << " " << massInjected_plot<< " "<< ConvMass<<" " << convMass << " " << prozent1 << " " << prozent2  <<std::endl;
      outputFile_.close();


        if (getParam<bool>("Output.PlotProfiles"))
        {
            bool plotThisColumn = false;
            Scalar timeStepIdx=timeStepIndex;
            //iterate over grid columns
            for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
            {
                //only plot profiles for specific columns and time steps; timesteps entsprechen ca. 1t_seg, 2 t_seg, 3 t_seg
                if (((columnIndex ==20|| columnIndex==50|| columnIndex==100|| columnIndex==500) && (timeStepIdx == 20 ||timeStepIdx == 202 || timeStepIdx == 396 || timeStepIdx == 619)))
                {
                    plotThisColumn = true;
                }

                if(plotThisColumn)
                {
                    //iterate over all cells in one grid column,
                    //calculate averageSatColumn, collect sat and relPerm

                    Scalar averageSatColumn = 0.0;
                    Scalar volumeColumn = 0.0;
                    Scalar averageTotalConcColumn = 0.0;
                    PhaseVector pRefColumn(1e100);
                    Scalar densityW=0;
                    Scalar densityN=0;
                    Scalar viscosityW=0;
                    Scalar viscosityN=0;
                    Scalar massfractionH2=0;
                    Scalar massfractionBrine=0;
                    Scalar MassfractionH2=0;
                    Scalar MassfractionBrine=0;
                    PhaseVector pmin(1e100);
                    PhaseVector pmax(0.0);
                    typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                    for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                    {
                        const GlobalPosition& globalPos = (it->second).geometry().center();

                    auto fvGeometry = localView(this->fvGridGeometry());
                    fvGeometry.bindElement(it->second);

                    auto elemVolVars = localView(gridVariables.curGridVolVars());
                    elemVolVars.bindElement(it->second, fvGeometry, curSol);
                    Scalar positionZ = globalPos[dim - 1];

                        for (auto&& scv : scvs(fvGeometry))
                        {
                            const auto& volVars = elemVolVars[scv];
                            const Scalar volume = (it->second).geometry().volume();
                            densityW= volVars.density(wPhaseIdx);
                            densityN= volVars.density(nPhaseIdx);
                            viscosityW= volVars.viscosity(wPhaseIdx);
                            viscosityN= volVars.viscosity(nPhaseIdx);

                            Scalar satW = volVars.saturation(wPhaseIdx);
                            Scalar relPermW = volVars.relativePermeability(wPhaseIdx);

                            if (satW==1)
                            { densityN=0;
                                viscosityN=0;
                            }

                            //write data to sat-plots
                            std::ostringstream oss;
                            oss << "satProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                            std::string fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ << " " << satW << std::endl;
                            outputFile_.close();

                            if (satW<1) //reale gasplumedist, plume fängt auf der Höhe an, wo der erste Wert reingeschrieben wird
                            {  std::ostringstream oss;
                                oss << "gasplumedist" << columnIndex << "-" << timeStepIdx << ".out";
                                std::string fileName = oss.str();
                                outputFile_.open(fileName, std::ios::app);
                                outputFile_ << positionZ  << " " << positionZ << std::endl;
                                outputFile_.close();
                            }

                            //write data to relPerm-plots
                            oss.str("");
                            oss << "relPermProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                            fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ << " " << relPermW << std::endl;
                            outputFile_.close();

                            //write data to viscosity-plots
                            oss.str("");
                            oss << "viscosity_" << columnIndex << "-"<< timeStepIdx << ".out";
                            fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ << " " << viscosityW << " " << viscosityN << std::endl;
                            outputFile_.close();

                            //write data to density-plots
                            oss.str("");
                            oss << "density_" << columnIndex << "-"<< timeStepIdx << ".out";
                            fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ << " " << densityW << " " << densityN << std::endl;
                            outputFile_.close();

                            averageSatColumn += satW * volume;
                            volumeColumn += volume;
                            averageTotalConcColumn += ((volVars.saturation(wPhaseIdx)*volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wCompIdx)*0.2)+(volVars.saturation(nPhaseIdx)*volVars.massFraction(nPhaseIdx,nCompIdx)*volVars.density(nCompIdx)*0.2)) * volume;
                            pmin[wPhaseIdx]= std::min(pmin[wPhaseIdx], volVars.pressure(wPhaseIdx));
                            pmin[nPhaseIdx]= std::min(pmin[wPhaseIdx], volVars.pressure(nPhaseIdx));
                            pmax[wPhaseIdx]= std::max(pmax[wPhaseIdx], volVars.pressure(wPhaseIdx));
                            pmax[nPhaseIdx]= std::max(pmax[wPhaseIdx], volVars.pressure(nPhaseIdx));
                            pRefColumn[wPhaseIdx] = 0.5*(pmin[wPhaseIdx]+pmax[wPhaseIdx]);//Mittelwert
                            pRefColumn[nPhaseIdx] = 0.5*(pmin[nPhaseIdx]+pmax[nPhaseIdx]);

                        }


                    }
                    averageSatColumn /= volumeColumn;
                    averageTotalConcColumn/=volumeColumn;
                    static const Scalar domainHeight = this->fvGridGeometry().bBoxMax()[dim-1];
                    Scalar gasPlumeDist = calculateGasPlumeDist(dummy_, averageSatColumn, curSol,
                                                                gridVariables);
                    Scalar gasPlumeDistConc = calculateGasPlumeDistConc(dummy_, pRefColumn, averageTotalConcColumn);
                    Scalar gasPlumeDistConcpmin = calculateGasPlumeDistConc(dummy_, pmin, averageTotalConcColumn);
                    Scalar gasPlumeDistConcpmax = calculateGasPlumeDistConc(dummy_, pmax, averageTotalConcColumn);


                    typename std::map<int, Element>::iterator is = mapColumns_.lower_bound(columnIndex);
                    for (; is != mapColumns_.upper_bound(columnIndex); ++is)
                    {
                        const GlobalPosition& globalPos = (is->second).geometry().center();

                        auto fvGeometry = localView(this->fvGridGeometry());
                        fvGeometry.bindElement(is->second);

                        auto elemVolVars = localView(gridVariables.curGridVolVars());
                        elemVolVars.bindElement(is->second, fvGeometry, curSol);

                        for (auto&& scv : scvs(fvGeometry))
                        {   const auto& volVars = elemVolVars[scv];
                            massfractionH2= volVars.massFraction(wPhaseIdx, nCompIdx);
                            massfractionBrine= volVars.massFraction(wPhaseIdx, wCompIdx);
                            MassfractionH2= volVars.massFraction(nPhaseIdx, nCompIdx);
                            MassfractionBrine= volVars.massFraction(nPhaseIdx, wCompIdx);

                            Scalar positionZ = globalPos[dim - 1];
                            //write data to massfraction-plots
                            oss.str("");
                            oss << "massFraction_in_Brine"<< columnIndex << "-" << timeStepIdx << ".out";
                            fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ << " " << massfractionH2 << " " << massfractionBrine << std::endl;
                            outputFile_.close();

                            //write data to massfraction-plots
                            oss.str("");
                            oss << "massFraction_in_H2"<< columnIndex << "-" << timeStepIdx << ".out";
                            fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ << " " << MassfractionH2 << " " << MassfractionBrine << std::endl;
                            outputFile_.close();

                            //write data to reconstructed gasplumedist plot
                            std::ostringstream oss;
                            oss << "gasplumedistrec" << columnIndex << "-" << timeStepIdx << ".out";
                            std::string fileName = oss.str();
                            outputFile_.open(fileName, std::ios::app);
                            outputFile_ << positionZ  <<gasPlumeDistConcpmin << " " <<gasPlumeDistConcpmax << " " << gasPlumeDistConc<< std::endl;
                            outputFile_.close();

                        }
                    }

                    //collect data for sat and relPerm VE profiles
                    static const int steps = 100;
                    static const Scalar deltaZ = domainHeight/steps;
                    for (int i = 0; i <= steps; i++)
                    {
                         const GlobalPosition& globalPos = (it->second).geometry().center();

                        Scalar positionZ2 = i*deltaZ;
                        Scalar reconstSatW = reconstSaturation(positionZ2, gasPlumeDist, curSol, gridVariables);
                        Scalar reconstRelPermW = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), reconstSatW);
                        Scalar reconstSatWConc = reconstSaturationConc(positionZ2, gasPlumeDistConc,curSol,gridVariables,pRefColumn);
                        Scalar reconstRelPermWConc = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), reconstSatWConc);
                        Scalar reconstSatWConcpmin = reconstSaturationConc(positionZ2, gasPlumeDistConcpmin,curSol,gridVariables,pmin);
                        Scalar reconstRelPermWConcpmin = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), reconstSatWConcpmin);
                        Scalar reconstSatWConcpmax = reconstSaturationConc(positionZ2, gasPlumeDistConcpmax,curSol,gridVariables,pmin);
                        Scalar reconstRelPermWConcpmax = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), reconstSatWConcpmax);

                        //write data to reconstSat-plots
                        std::ostringstream oss;
                        oss << "reconstSatProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                        std::string fileName = oss.str();
                        outputFile_.open(fileName, std::ios::app);
                        outputFile_ << positionZ2 << " " << reconstSatWConcpmin<< " " << reconstSatWConcpmax << " " << reconstSatWConc<< std::endl;
                        outputFile_.close();

                        //write data to reconstRelPerm-plots
                        oss.str("");
                        oss << "reconstRelPermProfile_" << columnIndex << "-" << timeStepIdx << ".out";
                        fileName = oss.str();
                        outputFile_.open(fileName, std::ios::app);
                        outputFile_ << positionZ2 <<  " " << reconstRelPermWConcpmin<<" " << reconstRelPermWConcpmax<< " " << reconstRelPermWConc << std::endl;
                        outputFile_.close();

                    }

                    plotThisColumn = false;
                }
            }

        }

        if (getParam<bool>("Output.PlotVECriteriaForColumns"))
        {
            const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
            bool plotThisColumn = false;
            //iterate over grid columns
            for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
            {
                //only plot VE criteria over time for specific columns
                if (columnIndex == 20 || columnIndex == 50||columnIndex == 100 || columnIndex ==500)
                {
                    plotThisColumn = true;
                }

                    if (plotThisColumn)
                    {
                        //iterate over all cells in one grid column and calculate averageSatColumn
                        Scalar averageSatColumn = 0;
                        Scalar volumeColumn = 0;
                        Scalar averageTotalConcColumn = 0.0;
                        PhaseVector pRefColumn(1e100);
                        PhaseVector pmin(1e100);
                        PhaseVector pmax(0.0);
                        Scalar viscosityW=0;
                        typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                        for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                        {
                            auto fvGeometry = localView(this->fvGridGeometry());
                            fvGeometry.bindElement(it->second);

                            auto elemVolVars = localView(gridVariables.curGridVolVars());
                            elemVolVars.bindElement(it->second, fvGeometry, curSol);

                            for (auto&& scv : scvs(fvGeometry))
                            {   const auto& volVars = elemVolVars[scv];
                                const Scalar volume = (it->second).geometry().volume();
                                const Scalar saturationW = volVars.saturation(wPhaseIdx);
                                averageSatColumn += saturationW * volume;
                                volumeColumn += volume;
                                averageTotalConcColumn += ((volVars.saturation(wPhaseIdx)*volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wCompIdx)*0.2)+(volVars.saturation(nPhaseIdx)*volVars.massFraction(nPhaseIdx,nCompIdx)*volVars.density(nCompIdx)*0.2)) * volume;

                                pmin[wPhaseIdx]= std::min(pmin[wPhaseIdx], volVars.pressure(wPhaseIdx));
                                pmin[nPhaseIdx]= std::min(pmin[wPhaseIdx], volVars.pressure(nPhaseIdx));
                                pmax[wPhaseIdx]= std::max(pmax[wPhaseIdx], volVars.pressure(wPhaseIdx));
                                pmax[nPhaseIdx]= std::max(pmax[wPhaseIdx], volVars.pressure(nPhaseIdx));
                                viscosityW= volVars.viscosity(wPhaseIdx);
                                pRefColumn[wPhaseIdx] = 0.5*(pmin[wPhaseIdx]+pmax[wPhaseIdx]);//Mittelwert
                                pRefColumn[nPhaseIdx] = 0.5*(pmin[nPhaseIdx]+pmax[nPhaseIdx]);
                            }

                        }
                        averageSatColumn /= volumeColumn;
                        averageTotalConcColumn/=volumeColumn;

                        Scalar gasPlumeDist = calculateGasPlumeDist(dummy_, averageSatColumn,curSol, gridVariables);
                        Scalar gasPlumeDistConc = calculateGasPlumeDistConc(dummy_, pRefColumn, averageTotalConcColumn);
                        Scalar gasPlumeDistConcpmin = calculateGasPlumeDistConc(dummy_, pmin, averageTotalConcColumn);
                        Scalar gasPlumeDistConcpmax = calculateGasPlumeDistConc(dummy_, pmax, averageTotalConcColumn);

                        //iterate over all cells in one grid column and calculate VE criteria
                        Scalar satCrit = 0;
                        Scalar relPermCrit = 0;
                        Scalar satCritConc=0.0;
                        Scalar relPermCritConc=0.0;

                        Scalar satCritConcpmin=0.0;
                        Scalar relPermCritConcpmax=0.0;
                        Scalar satCritConcpmax=0.0;
                        Scalar relPermCritConcpmin=0.0;
                        Scalar mobilitypmin=0;
                        Scalar mobilitypmax=0;
                        Scalar mobility=0;
                        static const Scalar domainHeight = this->fvGridGeometry().bBoxMax()[dim - 1];
                        static const Scalar deltaZ = domainHeight/numberOfCells[dim-1];
                        it = mapColumns_.lower_bound(columnIndex);
                        for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                        {
                            const GlobalPosition& globalPos = (it->second).geometry().center();

                            auto fvGeometry = localView(this->fvGridGeometry());
                            fvGeometry.bindElement(it->second);

                            auto elemVolVars = localView(gridVariables.curGridVolVars());
                            elemVolVars.bindElement(it->second, fvGeometry, curSol);

                            for (auto&& scv : scvs(fvGeometry))
                            {   const auto& volVars = elemVolVars[scv];
                                const Scalar lowerBound = globalPos[dim-1] - deltaZ/2.0;
                                const Scalar upperBound = globalPos[dim-1] + deltaZ/2.0;
                                const Scalar saturationW = volVars.saturation(wPhaseIdx);
                                satCrit += calculateSatCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist,curSol, gridVariables);
                                relPermCrit += calculateKrwCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist,curSol,gridVariables);
                                satCritConc += calculateSatCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConc,curSol, gridVariables, pRefColumn);
                                relPermCritConc += calculateKrwCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConc,curSol,gridVariables, pRefColumn);
                                satCritConcpmin += calculateSatCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmin,curSol, gridVariables, pmin);
                                satCritConcpmax += calculateSatCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmax,curSol, gridVariables, pmax);
                                relPermCritConcpmin += calculateKrwCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmin,curSol,gridVariables, pmin);
                                relPermCritConcpmax += calculateKrwCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmax,curSol,gridVariables, pmax);
                                mobilitypmin+=relPermCritConcpmin/viscosityW;
                                mobilitypmax+=relPermCritConcpmax/viscosityW;
                                mobility+=relPermCritConc/viscosityW;
                            }
                        }

                        //write data to sat-criterion plots
                        std::ostringstream oss;
                        oss << "satCritColumn_" << columnIndex << ".out";
                        std::string fileName = oss.str();
                        outputFile_.open(fileName, std::ios::app);
                        outputFile_ << time/segTime_ << " " << satCritConcpmin<< " " << satCritConcpmax<< " " << satCritConc << std::endl;
                        outputFile_.close();

                        //write data to relPerm-criterion plots
                        oss.str("");
                        oss << "relPermCritColumn_" << columnIndex << ".out";
                        fileName = oss.str();
                        outputFile_.open(fileName, std::ios::app);
                        outputFile_ << time/segTime_ << " "<< relPermCritConcpmin<< " " << relPermCritConcpmax<< " " << relPermCritConc <<" " << mobilitypmin << " " << mobilitypmax << " "<< mobility <<std::endl;
                        outputFile_.close();
                    }
                    plotThisColumn = false;
            }
            }

        if (getParam<bool>("Output.PlotVECriteriaOverDomain"))
        {
            bool plotCriteria = false;
            const Scalar timeStepIdx = timeStepIndex;
            //only plot VE criteria over domain for specific timeStepIdx
            if (timeStepIdx == 202 || timeStepIdx == 396 || timeStepIdx == 619)
            {
                plotCriteria = true;
            }

            if(plotCriteria)
            {
                const CellArray numberOfCells = getParam<CellArray>("Grid.Cells");
                bool plotThisColumn = false;
                //iterate over grid columns
                for (int columnIndex = 1; columnIndex <= numberOfCells[0]; ++columnIndex)
                {
                    //iterate over all cells in one grid column and calculate averageSatColumn
                    Scalar averageSatColumn = 0;
                    Scalar volumeColumn = 0;
                    Scalar averageTotalConcColumn = 0.0;
                    PhaseVector pRefColumn(1e100);
                    PhaseVector pmin(1e100);
                    PhaseVector pmax(0.0);
                    Scalar viscosityW=0;
                    typename std::map<int, Element>::iterator it = mapColumns_.lower_bound(columnIndex);
                    for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                    {
                         auto fvGeometry = localView(this->fvGridGeometry());
                        fvGeometry.bindElement(it->second);

                        auto elemVolVars = localView(gridVariables.curGridVolVars());
                        elemVolVars.bindElement(it->second, fvGeometry, curSol);

                        for (auto&& scv : scvs(fvGeometry))
                        {
                        const auto& volVars = elemVolVars[scv];
                        const Scalar volume = (it->second).geometry().volume();
                        const Scalar saturationW = volVars.saturation(wPhaseIdx);
                        averageSatColumn += saturationW * volume;
                        volumeColumn += volume;
                        averageTotalConcColumn += ((volVars.saturation(wPhaseIdx)*volVars.massFraction(wPhaseIdx,nCompIdx)*volVars.density(wCompIdx)*0.2)+(volVars.saturation(nPhaseIdx)*volVars.massFraction(nPhaseIdx,nCompIdx)*volVars.density(nCompIdx)*0.2)) * volume;
                        pmin[wPhaseIdx]= std::min(pmin[wPhaseIdx], volVars.pressure(wPhaseIdx));
                        pmin[nPhaseIdx]= std::min(pmin[wPhaseIdx], volVars.pressure(nPhaseIdx));
                        pmax[wPhaseIdx]= std::max(pmax[wPhaseIdx], volVars.pressure(wPhaseIdx));
                        pmax[nPhaseIdx]= std::max(pmax[wPhaseIdx], volVars.pressure(nPhaseIdx));
                        viscosityW= volVars.viscosity(wPhaseIdx);
                        pRefColumn[wPhaseIdx] = 0.5*(pmin[wPhaseIdx]+pmax[wPhaseIdx]);//Mittelwert
                        pRefColumn[nPhaseIdx] = 0.5*(pmin[nPhaseIdx]+pmax[nPhaseIdx]);
                        }
                    }
                    averageSatColumn /= volumeColumn;
                    averageTotalConcColumn/=volumeColumn;

                    Scalar gasPlumeDist = calculateGasPlumeDist(dummy_, averageSatColumn, curSol, gridVariables);
                    Scalar gasPlumeDistConc = calculateGasPlumeDistConc(dummy_, pRefColumn, averageTotalConcColumn);

                    Scalar gasPlumeDistConcpmin = calculateGasPlumeDistConc(dummy_, pmin, averageTotalConcColumn);
                    Scalar gasPlumeDistConcpmax = calculateGasPlumeDistConc(dummy_, pmax, averageTotalConcColumn);

                    //iterate over all cells in one grid column and calculate VE criteria
                    Scalar positionX;
                    Scalar satCrit = 0;
                    Scalar relPermCrit = 0;
                    Scalar satCritConc = 0;
                    Scalar relPermCritConc = 0;
                    Scalar satCritConcpmin = 0;
                    Scalar relPermCritConcpmin = 0;
                    Scalar satCritConcpmax = 0;
                    Scalar relPermCritConcpmax = 0;
                    Scalar mobilitypmin=0.0;
                    Scalar mobilitypmax=0.0;
                    Scalar mobility=0.0;

                    static const Scalar domainHeight = this->fvGridGeometry().bBoxMax()[dim - 1];
                    static const Scalar deltaZ = domainHeight/numberOfCells[dim-1];
                    it = mapColumns_.lower_bound(columnIndex);
                    for (; it != mapColumns_.upper_bound(columnIndex); ++it)
                    {   const GlobalPosition& globalPos = (it->second).geometry().center();
                         auto fvGeometry = localView(this->fvGridGeometry());
                        fvGeometry.bindElement(it->second);

                        auto elemVolVars = localView(gridVariables.curGridVolVars());
                        elemVolVars.bindElement(it->second, fvGeometry, curSol);

                        for (auto&& scv : scvs(fvGeometry))
                        {
                            const auto& volVars = elemVolVars[scv];
                            positionX = globalPos[0];
                            const Scalar lowerBound = globalPos[dim-1] - deltaZ/2.0;
                            const Scalar upperBound = globalPos[dim-1] + deltaZ/2.0;
                            const Scalar saturationW = volVars.saturation(wPhaseIdx);
                            satCrit += calculateSatCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist,curSol, gridVariables);
                            relPermCrit += calculateKrwCriterionIntegral(lowerBound, upperBound, saturationW, gasPlumeDist,curSol,gridVariables);
                            satCritConc += calculateSatCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConc,curSol, gridVariables, pRefColumn);
                            relPermCritConc += calculateKrwCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConc,curSol,gridVariables, pRefColumn);
                            satCritConcpmin += calculateSatCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmin,curSol, gridVariables, pmin);
                            satCritConcpmax += calculateSatCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmin,curSol, gridVariables, pmax);
                            relPermCritConcpmin += calculateKrwCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmin,curSol,gridVariables, pmin);
                            relPermCritConcpmax += calculateKrwCriterionIntegralConc(lowerBound, upperBound, saturationW, gasPlumeDistConcpmax,curSol,gridVariables, pmax);
                            mobilitypmin+=relPermCritConcpmin/viscosityW;
                            mobilitypmax+=relPermCritConcpmax/viscosityW;
                            mobility+=relPermCritConc/viscosityW;
                        }
                    }

                    satCrit /= (domainHeight-gasPlumeDist);
                    relPermCrit /= (domainHeight-gasPlumeDist);
                    satCritConc /= (domainHeight-gasPlumeDistConc);
                    relPermCritConc /= (domainHeight-gasPlumeDistConc);

                    satCritConcpmin /= (domainHeight-gasPlumeDistConcpmin);
                    relPermCritConcpmin /= (domainHeight-gasPlumeDistConcpmin);

                    satCritConcpmax /= (domainHeight-gasPlumeDistConcpmax);
                    relPermCritConcpmax /= (domainHeight-gasPlumeDistConcpmax);

                    if(averageSatColumn > 1.0-eps_)
                    {
                        satCrit = 0;
                        relPermCrit = 0;
                        satCritConc = 0;
                        relPermCritConc = 0;

                        Scalar satCritConcpmin = 0;
                        Scalar relPermCritConcpmin = 0;
                        Scalar satCritConcpmax = 0;
                        Scalar relPermCritConcpmax = 0;
                    }

                    //write data to satCrit-plots
                    std::ostringstream oss;
                    oss << "satCrit_" << timeStepIdx << ".out";
                    std::string fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionX << " " << satCritConcpmin << " " << satCritConcpmax << " " << satCritConc<< std::endl;
                    outputFile_.close();

                    //write data to relPermCrit-plots
                    oss.str("");
                    oss << "relPermCrit_" << timeStepIdx << ".out";
                    fileName = oss.str();
                    outputFile_.open(fileName, std::ios::app);
                    outputFile_ << positionX << " "  << relPermCritConcpmin<<  " " << relPermCritConcpmax<< " " << relPermCritConc<< " " << mobilitypmin << " " << mobilitypmax <<" " << mobility << std::endl;
                    outputFile_.close();
                }
            }


            plotCriteria = false;
        }
    }


    /*! \brief Calculates gasPlumeDist, distance of gas plume from bottom, 2-phase flow
     *
     * Stores minGasPlumeDist for all grid cells
     */
    Scalar calculateGasPlumeDist(const Element& element, Scalar satW,const SolutionVector& curSol,
                      const GridVariables& gridVariables)
    {
        const GlobalPosition& globalPos = dummy_.geometry().center();

        Scalar domainHeight = this->fvGridGeometry().bBoxMax()[dim - 1];
        Scalar resSatW = this->spatialParams().materialLawParamsAtPos(globalPos).swr();
        Scalar resSatN = this->spatialParams().materialLawParamsAtPos(globalPos).snr();
        Scalar gravity = this->gravity().two_norm();

        Scalar gasPlumeDist = 0.0;

        if (veModel_ == 0) //calculate gasPlumeDist for sharp interface ve model
        {
            gasPlumeDist = domainHeight * (satW - resSatW) / (1.0 - resSatW);
        }

        else if (veModel_ == 1) //calculate gasPlumeDist for capillary fringe model
        {
             const GlobalPosition& globalPos = dummy_.geometry().center();
            Scalar pRef = pressure(globalPos);
            Scalar tempRef = temperature();
            Scalar densityW = WettingPhase::liquidDensity(tempRef, pRef);
            Scalar densityNw = NonWettingPhase::gasDensity(tempRef, pRef);
            Scalar lambda = this->spatialParams().materialLawParamsAtPos(globalPos).lambda();
            Scalar entryP = this->spatialParams().materialLawParamsAtPos(globalPos).pe();

            //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
            Scalar fullIntegral =
            1.0 / (1.0 - lambda) * std::pow((domainHeight * (densityW - densityNw) * gravity + entryP),(1.0 - lambda))
            * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
            + resSatW * domainHeight
            - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityNw) * gravity);
            Scalar residual = 1.0;

            //GasPlumeDist>0
            if (fullIntegral < satW * domainHeight)
            {
                gasPlumeDist = domainHeight / 2.0; //XiStart
                //solve equation for
                int count = 0;
                for (; count < 100; count++)
                {
                    residual =
                    1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP),(1.0 - lambda))
                    * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
                    + resSatW * (domainHeight - gasPlumeDist)
                    - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityNw) * gravity)
                    + gasPlumeDist
                    - satW * domainHeight;


                    if (fabs(residual) < 1e-12)
                    {
                        break;
                    }

                    Scalar derivation =
                    std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP), -lambda)
                    * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda) - resSatW + 1.0;


                    gasPlumeDist = gasPlumeDist - residual / (derivation);
                }
            }
            //GasPlumeDist<0
            else if (fullIntegral > satW * domainHeight)
            {
                gasPlumeDist = 0.0; //XiStart
                //solve equation
                int count = 0;
                for (; count < 100; count++)
                {
                    residual =
                    1.0 / (1.0 - lambda)
                    * std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP), (1.0 - lambda))
                    * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityNw) * gravity)
                    + resSatW * domainHeight
                    - 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDist * (densityW - densityNw)
                    * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                    / ((densityW - densityNw) * gravity)
                    - satW * domainHeight;


                    if (fabs(residual) < 1e-10)
                    {
                        break;
                    }

                    Scalar derivation =
                    std::pow(((domainHeight - gasPlumeDist) * (densityW - densityNw) * gravity + entryP), -lambda)
                    * (- 1.0 + resSatN + resSatW) * std::pow(entryP, lambda)
                    + std::pow((entryP - gasPlumeDist * (densityW - densityNw) * gravity), -lambda)
                    * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda);

                    gasPlumeDist = gasPlumeDist - residual / (derivation);
                }
            }
            //GasPlumeDist=0
            else
            {
                gasPlumeDist = 0.0;
            }
        }

        return gasPlumeDist;

    }


    /*! \brief Calculates gasPlumeDist, distance of gas plume from bottom, 2-phase compositional flow
     *
     * Stores minGasPlumeDist for all grid cells
     */

    Scalar calculateGasPlumeDistConc(const Element element, PhaseVector pressures, Scalar averageTotalConcColumn)
    {
        FluidState fluidState;
        GlobalPosition globalPos = element.geometry().center();
        Scalar domainHeight = this->fvGridGeometry().bBoxMax()[dim - 1];
        Scalar porosity = this->spatialParams().porosityAtPos(globalPos);
        Scalar temperature = this->temperature();
        Scalar resSatW = this->spatialParams().materialLawParamsAtPos(globalPos).swr();
        Scalar resSatN = this->spatialParams().materialLawParamsAtPos(globalPos).snr();
        Scalar gravity = this->gravity().two_norm();

        Dumux::CompositionalFlash<Scalar, FluidSystem> flash;
        flash.saturationFlash2p2c(fluidState, resSatW, pressures, porosity, temperature);

        Scalar X_g_gComp=fluidState.massFraction(nPhaseIdx,nCompIdx);
        Scalar X_l_gComp=fluidState.massFraction(wPhaseIdx,nCompIdx);
        Scalar densityN=fluidState.density(nPhaseIdx);
        Scalar densityW=fluidState.density(wPhaseIdx);


        Scalar gasPlumeDistConc = 0.0;

        if (veModel_ == 0) //calculate gasPlumeDist for sharp interface ve model
        {
            gasPlumeDistConc = domainHeight - (domainHeight * averageTotalConcColumn /
            (( porosity* X_g_gComp * densityN * (1-resSatW)) + (porosity* X_l_gComp * densityW * resSatW)));
            std::cout << X_g_gComp << " " << X_l_gComp << std::endl;
            std::cout << densityN << " " << densityW << std::endl;
            std::cout << domainHeight << " " << averageTotalConcColumn << std::endl;
        }

            else if (veModel_ == 1) //calculate gasPlumeDist for capillary fringe model
            {
                Scalar lambda = this->spatialParams().materialLawParamsAtPos(globalPos).lambda();
                Scalar entryP = this->spatialParams().materialLawParamsAtPos(globalPos).pe();
                Scalar porosity= this->spatialParams().porosityAtPos(globalPos);

                //check if GasPlumeDist>0, GasPlumeDist=0, GasPlumeDist<0
                Scalar fullIntegral =
                (domainHeight-1.0 / (1.0 - lambda) * std::pow((domainHeight * (densityW - densityN) * gravity + entryP),(1.0 - lambda))
                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                - resSatW * domainHeight
                + entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))*porosity*densityN*X_g_gComp+
                (1.0 / (1.0 - lambda) * std::pow((domainHeight * (densityW - densityN) * gravity + entryP),(1.0 - lambda))
                * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                + resSatW * domainHeight
                - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity))*porosity*densityW*X_l_gComp;


                Scalar residual = 1.0;

                //GasPlumeDist>0
                if (fullIntegral > averageTotalConcColumn * domainHeight)
                {
                    gasPlumeDistConc = domainHeight / 2.0; //XiStart
                    //solve equation for
                    int count = 0;
                    for (; count < 100; count++)
                    {
                        residual =
                        (domainHeight- 1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP),(1.0 - lambda))
                        * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                        - resSatW * domainHeight

                        + entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity)+gasPlumeDistConc*(-1+resSatW))*(porosity*densityN*X_g_gComp)+

                        (+1.0 / (1.0 - lambda) * std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP),(1.0 - lambda))
                        * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                        + resSatW * (domainHeight - gasPlumeDistConc)

                        - entryP * (1.0 - resSatW - resSatN) / ((1.0 - lambda) * (densityW - densityN) * gravity) )*(porosity*densityW*X_l_gComp)
                        - averageTotalConcColumn * domainHeight;


                        if (fabs(residual) < 1e-12)
                        {
                            break;
                        }

                        Scalar derivation =
                        (std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), -lambda)
                        * ( 1.0 - resSatN - resSatW) * std::pow(entryP, lambda) + resSatW -1.0)*(porosity*densityN*X_g_gComp)+

                        (-std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), -lambda)
                        * ( 1.0 - resSatN - resSatW) * std::pow(entryP, lambda) - resSatW )*(porosity*densityW*X_l_gComp);


                        gasPlumeDistConc = gasPlumeDistConc - residual / (derivation);
                    }
                }
                //GasPlumeDist<0
                else if (fullIntegral < averageTotalConcColumn * domainHeight)
                {
                    gasPlumeDistConc = 0.0; //XiStart
                    //solve equation
                    int count = 0;
                    for (; count < 100; count++)
                    {
                        residual =

                        (domainHeight-1.0 / (1.0 - lambda)
                        * std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), (1.0 - lambda))
                        * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                        - resSatW * domainHeight)*porosity*densityN*X_g_gComp+

                        ( 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDistConc * (densityW - densityN)
                        * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                        / ((densityW - densityN) * gravity))*porosity*densityN*X_g_gComp

                        +(1.0 / (1.0 - lambda)
                        * std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), (1.0 - lambda))
                        * (1.0 - resSatW - resSatN)* std::pow(entryP, lambda) / ((densityW - densityN) * gravity)
                        + resSatW * domainHeight)*(porosity*densityW*X_l_gComp)+

                        (- 1.0 / (1.0 - lambda) * std::pow((entryP - gasPlumeDistConc * (densityW - densityN)
                        * gravity),(1.0 - lambda)) * (1.0 - resSatW - resSatN) * std::pow(entryP, lambda)
                        / ((densityW - densityN) * gravity))*(porosity*densityW*X_l_gComp)

                        - averageTotalConcColumn * domainHeight;


                        if (fabs(residual) < 1e-10)
                        {
                            break;
                        }

                        Scalar derivation =

                        (std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), -lambda)
                        * ( 1.0 - resSatN - resSatW) * std::pow(entryP, lambda))*porosity* densityN*X_g_gComp+

                        (- std::pow((entryP - gasPlumeDistConc * (densityW - densityN) * gravity), -lambda)
                        * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda))*porosity* densityN*X_g_gComp

                        +(-std::pow(((domainHeight - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), -lambda)
                        * ( 1.0 - resSatN - resSatW) * std::pow(entryP, lambda))*porosity* densityW*X_l_gComp

                        + (std::pow((entryP - gasPlumeDistConc * (densityW - densityN) * gravity), -lambda)
                        * (1.0 - resSatN - resSatW) * std::pow(entryP, lambda))*porosity* densityW*X_l_gComp;

                        gasPlumeDistConc = gasPlumeDistConc - residual / (derivation);
                    }
                }
                //GasPlumeDist=0
                else
                {
                    gasPlumeDistConc = 0.0;
                }

            }


    std::cout << " xi" << gasPlumeDistConc << " "<< std::endl;
        return gasPlumeDistConc;
    }


    /*! \brief Calculates integral of difference of wetting saturation over z
     *
     */
Scalar calculateSatCriterionIntegral(Scalar lowerBound, Scalar upperBound, Scalar satW, Scalar gasPlumeDist,const SolutionVector& curSol,const GridVariables& gridVariables)
    {
        int intervalNumber = 10;
        Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

        Scalar satIntegral = 0.0;
        for(int count=0; count<intervalNumber; count++ )
        {
            satIntegral += std::abs((reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist,curSol,gridVariables)
                    + reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist,curSol,gridVariables))/2.0 - satW);
        }
        satIntegral = satIntegral * deltaZ;

        return satIntegral;
    }

    /*! \brief Calculates integral of difference of wetting saturation over z for compositional flow
     *
     */
Scalar calculateSatCriterionIntegralConc(Scalar lowerBound, Scalar upperBound, Scalar satW, Scalar gasPlumeDistConc,const SolutionVector& curSol,const GridVariables& gridVariables, PhaseVector pressures)
    {
        int intervalNumber = 10;
        Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;

        Scalar satIntegral = 0.0;
        for(int count=0; count<intervalNumber; count++ )
        {
            satIntegral += std::abs((reconstSaturationConc(lowerBound + count*deltaZ, gasPlumeDistConc,curSol,gridVariables, pressures)
            + reconstSaturationConc(lowerBound + (count+1)*deltaZ, gasPlumeDistConc,curSol,gridVariables, pressures))/2.0 - satW);
        }
        satIntegral = satIntegral * deltaZ;

        return satIntegral;
    }


    /*! \brief Calculates integral of difference of relative wetting permeability over z
     *
     */
Scalar calculateKrwCriterionIntegral(Scalar lowerBound, Scalar upperBound, Scalar satW, Scalar gasPlumeDist,const SolutionVector& curSol, const GridVariables& gridVariables)
    {   Scalar krwIntegral = 0.0;
        int intervalNumber = 10;
        Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;
        GlobalPosition globalPos = dummy_.geometry().center();

        for(int count=0; count<intervalNumber; count++ )
        {
            Scalar sat1 = reconstSaturation(lowerBound + count*deltaZ, gasPlumeDist,curSol,gridVariables);
            Scalar sat2 = reconstSaturation(lowerBound + (count+1)*deltaZ, gasPlumeDist,curSol,gridVariables);
            Scalar krw1 = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), sat1);
            Scalar krw2 = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), sat2);
            Scalar krw = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), satW);
            krwIntegral += std::abs((krw1 + krw2)/2.0 - krw);
        }
        krwIntegral = krwIntegral * deltaZ;
        return krwIntegral;
    }


    /*! \brief Calculates integral of difference of relative wetting permeability over z for compositional flow
     *
     */
Scalar calculateKrwCriterionIntegralConc(Scalar lowerBound, Scalar upperBound, Scalar satW, Scalar gasPlumeDist,const SolutionVector& curSol, const GridVariables& gridVariables, PhaseVector pressures)
    {   Scalar krwIntegral = 0.0;
        int intervalNumber = 10;
        Scalar deltaZ = (upperBound - lowerBound)/intervalNumber;
        GlobalPosition globalPos = dummy_.geometry().center();

        for(int count=0; count<intervalNumber; count++ )
        {
            Scalar sat1 = reconstSaturationConc(lowerBound + count*deltaZ, gasPlumeDist,curSol,gridVariables, pressures);
            Scalar sat2 = reconstSaturationConc(lowerBound + (count+1)*deltaZ, gasPlumeDist,curSol,gridVariables, pressures);
            Scalar krw1 = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), sat1);
            Scalar krw2 = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), sat2);
            Scalar krw = Materiallaw::krw(this->spatialParams().materialLawParamsAtPos(globalPos), satW);
            krwIntegral += std::abs((krw1 + krw2)/2.0 - krw);
        }
        krwIntegral = krwIntegral * deltaZ;
        return krwIntegral;
    }


    /*! \brief Calculates reconstructed saturation for VE column, dependent on z
     *
     */
    Scalar reconstSaturation(Scalar height, Scalar gasPlumeDist,const SolutionVector& curSol,
                      const GridVariables& gridVariables)
    {
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar pRef = this->pressure(globalPos);
        Scalar tempRef = this->temperature();
        Scalar resSatW = this->spatialParams().materialLawParamsAtPos(globalPos).swr();
        Scalar resSatN = this->spatialParams().materialLawParamsAtPos(globalPos).snr();
        Scalar densityW = WettingPhase::liquidDensity(tempRef, pRef);
        Scalar densityNw = NonWettingPhase::gasDensity(tempRef, pRef);
        Scalar entryP = this->spatialParams().materialLawParamsAtPos(globalPos).pe();
        Scalar lambda = this->spatialParams().materialLawParamsAtPos(globalPos).lambda();


        Scalar reconstSaturation = 0.0;

        if (veModel_ == 0) //reconstruct phase saturation for sharp interface ve model
        {
            reconstSaturation = 1.0;
            if(height > gasPlumeDist)
            {
                reconstSaturation = resSatW;
            }
        }
        else if (veModel_ == 1) //reconstruct phase saturation for capillary fringe model
        {
            reconstSaturation = 1.0;
            if(height > gasPlumeDist)
            {
                reconstSaturation = std::pow(((height - gasPlumeDist) * (densityW - densityNw) * this->gravity().two_norm() + entryP), (-lambda))
                * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
            }
        }

        return reconstSaturation;
    }


    /*! \brief Calculates reconstructed saturation for VE column, dependent on z for compositional flow
     *
     */
    Scalar reconstSaturationConc(Scalar height, Scalar gasPlumeDistConc,const SolutionVector& curSol,
                                 const GridVariables& gridVariables, PhaseVector pressures)
    {
        GlobalPosition globalPos = dummy_.geometry().center();
        Scalar tempRef = this->temperature();
        Scalar resSatW = this->spatialParams().materialLawParamsAtPos(globalPos).swr();
        Scalar resSatN = this->spatialParams().materialLawParamsAtPos(globalPos).snr();
        Scalar entryP = this->spatialParams().materialLawParamsAtPos(globalPos).pe();
        Scalar lambda = this->spatialParams().materialLawParamsAtPos(globalPos).lambda();
        FluidState fluidState;
        Scalar porosity = this->spatialParams().porosityAtPos(globalPos);
        Scalar gravity = this->gravity().two_norm();

        Dumux::CompositionalFlash<Scalar, FluidSystem> flash;
        flash.saturationFlash2p2c(fluidState, resSatW, pressures, porosity, tempRef);

        Scalar densityN=fluidState.density(nPhaseIdx);
        Scalar densityW=fluidState.density(wPhaseIdx);

        Scalar reconstSaturationConc = 0.0;

        if (veModel_ == 0) //reconstruct phase saturation for sharp interface ve model
        {
            reconstSaturationConc = 1.0;
            if(height > gasPlumeDistConc)
            {
                reconstSaturationConc = resSatW;
            }
        }
        else if (veModel_ == 1) //reconstruct phase saturation for capillary fringe model
        {
            reconstSaturationConc = 1.0;
            if(height > gasPlumeDistConc)
            {
                reconstSaturationConc = std::pow(((height - gasPlumeDistConc) * (densityW - densityN) * gravity + entryP), (-lambda))
                * std::pow(entryP, lambda) * (1.0 - resSatW - resSatN) + resSatW;
            }
        }

        return reconstSaturationConc;
    }


private:
    /*!
     * \brief Evaluates the initial values for a control volume.
     *
     * The internal method for the initial condition
     *
     * \param globalPos The global position
     */
    
PrimaryVariables initial_(const GlobalPosition &globalPos) const
    {
        PrimaryVariables priVars(0.0);
        priVars.setState(wPhaseOnly);

        Scalar densityW = WettingPhase::liquidDensity(temperature_, 1e6);
        priVars[pressureIdx] = 1e5 + (maxDepth_ - globalPos[1])*densityW*9.81; //in 500m tiefe
        priVars[switchIdx] = 0.0;  //wPhaseOnly --> switch heißt massfraction of H2 in wPhase

        return priVars;
     }


    Scalar inflow;
    Scalar maxDepth_;
    std::string name_;
    Scalar massInjected_;
    Scalar mutable injectionPressure_;
    Scalar massInjectedTimestep_;
    Dumux::GnuplotInterface<double> gnuplot_;
    Dumux::GnuplotInterface<double> gnuplot2_;
    bool mutable reachedLowerBound_;

    Scalar temperature_;
    Scalar depthBOR_;
    static constexpr Scalar eps_ = 1e-6;

    int nTemperature_;
    int nPressure_;
    Scalar pressureLow_, pressureHigh_;
    Scalar temperatureLow_, temperatureHigh_;
    std::multimap<int, Element> mapColumns_;
    int numberOfColumns_;
    std::ofstream outputFile_;
    std::ofstream OutputFile_;
    std::vector<double> Kxx_;
    std::vector<double> Kyy_;
    std::vector<double> D_h2_;
    std::vector<double> mu_Brine_;
    std::vector<double> C_H2_;
    int veModel_;
    Element dummy_;
    Scalar CTZ_;
    Scalar segTime_;
    Scalar viscosityW_;

};



}// end namespace Dumux

#endif
